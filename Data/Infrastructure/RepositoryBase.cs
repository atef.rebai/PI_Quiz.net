﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PI_Quiz.Data.Infrastructure
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private MyContext dataContext;
        private MyContext DataContext { get { return dataContext = dbFactory.DataContext; } }
        private readonly IDbSet<T> dbset;
        private IDataBaseFactory dbFactory;
        public RepositoryBase(IDataBaseFactory dbFactory)
        {
            this.dbFactory = dbFactory;
           // dataContext = factory;
           // dataContext = new MyContext();
            //charger dbset avec les entités du data context
            dbset = this.DataContext.Set<T>();

        }

       
        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> condition)
        {
            foreach (T obj in dbset.Where(condition).AsEnumerable())

                dbset.Remove(obj);
        }

        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }

        public virtual T Get(Expression<Func<T, bool>> condition)
        {
            return dbset.Where(condition).FirstOrDefault();
        }

        public virtual T GetById(string id)
        {
            return dbset.Find(id);
        }

        public virtual T GetById(long id)
        {
            return dbset.Find(id);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> condition = null, Expression<Func<T, bool>> orderby = null)
        {
            IQueryable<T> Query = dbset;
            if (condition != null) { 
                Query = Query.Where(condition);
            }
            if (orderby != null) { 
                Query = Query.OrderBy(condition);
            }
            return Query.AsEnumerable();
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
