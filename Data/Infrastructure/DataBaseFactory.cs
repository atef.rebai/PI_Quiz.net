﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PI_Quiz.Data.Infrastructure
{
    public class DataBaseFactory : Disposable, IDataBaseFactory
    {
        private MyContext dataContext;
        public MyContext DataContext { get { return dataContext; } }

        public DataBaseFactory()
        {
            dataContext = new MyContext();
        }
        
        protected override void DisposeCore()
        {
            if (DataContext != null)
            DataContext.Dispose();
        }
    }
}
