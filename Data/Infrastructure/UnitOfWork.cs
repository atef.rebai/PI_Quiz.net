﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PI_Quiz.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDataBaseFactory dbFactory;
        public UnitOfWork(IDataBaseFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }
        public void Commit()
        {
            dbFactory.DataContext.SaveChanges();       }

        public void Dispose()
        {
            dbFactory.DataContext.Dispose();
        }

        public IRepositoryBase<T> GetRepository<T>() where T : class
        {
            return new RepositoryBase<T>(dbFactory);
        }
    }
}
