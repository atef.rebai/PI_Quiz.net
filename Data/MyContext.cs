﻿using Data.Configurations;
using Domain.Entities;
using PI_Quiz.Data.CustomConvention;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{

    public class MyContext : DbContext
    {
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<EventParticipations> EventParticipations { get; set; }


        public MyContext() : base("name=quizzdb_amazone")
        {
            //quizzdb_amazone for amazone tenant
            //quizzdb for localdb

            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<MyContext>(new MyContextInitialize());
        }

       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EventConfiguration());
            modelBuilder.Configurations.Add(new PostConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Configurations.Add(new TopicConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new QuizConfiguration());
            modelBuilder.Configurations.Add(new LessonConfiguration());
            modelBuilder.Configurations.Add(new EventParticipationsConfiguration());


            modelBuilder.Conventions.Add(new DateTime2Convention());

        }

        public class MyContextInitialize : DropCreateDatabaseIfModelChanges<MyContext>
        {
            protected override void Seed(MyContext context)
            {
                Tenant t = new Tenant()
                {
                    Id = 1,
                    Name = "esprit",
                    WebSiteURL = "www.esprit.tn",
                    CustemColor = "",
                    LogoURI = "1.jpg"
                };
                User u = new User()
                {
                    Id = 1,
                    Name = "esprit",
                    LastName = "esprit",
                    Password = "esprit",
                    Email = "esprit@esprit.tn",
                    ProfileImageURL = "https://s3-eu-west-1.amazonaws.com/bucketpi/u_1.jpg",
                    Role = "Admin",
                    State = "Active",
                    username= "esprit",
                    
                };

                Tenant t2 = new Tenant()
                {
                    Id = 1,
                    Name = "ISI",
                    WebSiteURL = "www.ISI.tn",
                    CustemColor = "",
                    LogoURI = "2.jpg"
                };
                User u2 = new User()
                {
                    Id = 1,
                    Name = "ISI",
                    LastName = "isi",
                    Password = "isi",
                    Email = "isi@isi.tn",
                    ProfileImageURL = "https://s3-eu-west-1.amazonaws.com/bucketpi/u_2.jpg",
                    Role = "Admin",
                    State = "Active",
                    username = "isi",

                };


                u.Tenant = t;
                u2.Tenant = t2;

                context.Tenants.Add(t);
                context.Users.Add(u);

                context.Tenants.Add(t2);
                context.Users.Add(u2);



                Event e = new Event()
                {
                    Id = 1,
                    Title = "formation python",
                    Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    DateCreation = DateTime.Now,
                    EndDate=DateTime.Now.AddDays(30),
                    ImageURI= "https://s3-eu-west-1.amazonaws.com/bucketpi/1.jpg",
                    Latitude= (float)36.86671,
                    Longitude= (float)10.3374481,
                    Location= "Rue Ibn Rochd, Site archéologique de Carthage, Tunisia",
                    NbPlaces=30,
                };

                Event e2 = new Event()
                {
                    Id = 1,
                    Title = "formation .net",
                    Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    DateCreation = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(30),
                    ImageURI = "https://s3-eu-west-1.amazonaws.com/bucketpi/1.jpg",
                    Latitude = (float)36.86691,
                    Longitude = (float)10.3374681,
                    Location = "Rue Ibn Rochd, Site archéologique de Carthage, Tunisia",
                    NbPlaces=30,
                };

                e.User = u;
                e2.User = u2;

                context.Events.Add(e);
                context.Events.Add(e2);




                context.SaveChanges();
                
                
            }

        }
    }


}
