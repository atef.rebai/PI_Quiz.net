﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    class LessonConfiguration : EntityTypeConfiguration<Lesson>
    {
        public LessonConfiguration()
        {
            HasMany(i => i.Certificates).WithRequired(i => i.Lesson).HasForeignKey(i => i.LessonId).WillCascadeOnDelete(false);
        }
    }
}
