﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    class EventParticipationsConfiguration : EntityTypeConfiguration<EventParticipations>
    {
        public EventParticipationsConfiguration()
        {
            HasRequired(x => x.Event).WithMany(x => x.ParticipatingUsers).HasForeignKey(x => x.EventId).WillCascadeOnDelete(true);
            HasRequired(x => x.User).WithMany(x => x.EventParticipations).HasForeignKey(x => x.UserId).WillCascadeOnDelete(true);
            HasKey(x => new { x.EventId, x.UserId });
        }

    }
}
