﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
  public   class TopicConfiguration : EntityTypeConfiguration<Topic>
    {
        public TopicConfiguration()
        {
            // one to many : Comment
            HasMany(u => u.Comments).WithRequired(x => x.Topic).HasForeignKey(m => m.TopicId).WillCascadeOnDelete(false);
        }
    }
}
