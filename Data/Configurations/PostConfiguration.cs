﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    public class PostConfiguration : EntityTypeConfiguration<Post>
    {
        public PostConfiguration()
        {
            // one to many : Question 
            HasMany(u => u.Questions).WithRequired(x => x.Post).HasForeignKey(m => m.PostId).WillCascadeOnDelete(false);
        }
    }
}
