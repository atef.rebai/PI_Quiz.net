﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    public class EventConfiguration : EntityTypeConfiguration<Event>
    {
        public EventConfiguration()
        {
            //one to one : poll
            HasOptional(c => c.Poll).WithOptionalPrincipal(m => m.Event);
            HasMany(x => x.EventComments).WithRequired(x => x.Event).HasForeignKey(x => x.EventId).WillCascadeOnDelete(true);
        }
    }
}
