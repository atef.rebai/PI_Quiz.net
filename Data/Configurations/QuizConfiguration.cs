﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
   public  class QuizConfiguration : EntityTypeConfiguration<Quiz>
    {

        public QuizConfiguration()
        {
            
            //many to one : lesson
            HasOptional(p => p.Lesson).WithMany(c => c.Quizs).HasForeignKey(p => p.LessonId).WillCascadeOnDelete(false);


        }


    }
}
