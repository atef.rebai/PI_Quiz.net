﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        { //many to one : tenant
            HasRequired(p => p.Tenant).WithMany(c => c.Users).HasForeignKey(p => p.TenantId).WillCascadeOnDelete(false);
            // one to many : Topic 
            HasMany(u => u.Topics).WithRequired(x => x.Publisher).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);
            // one to many : Certificate 
            HasMany(u => u.Certificates).WithRequired(x => x.User).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);
            // one to many : comment 
            HasMany(u => u.Comments).WithRequired(x => x.User).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);
            // one to many : Post 
            HasMany(u => u.CreatedPosts).WithRequired(x => x.Publisher).HasForeignKey(m => m.PublisherId).WillCascadeOnDelete(false);
            // many to many : Post
            HasMany(u => u.AuthorizedPosts).WithMany(p => p.AuthorizedUsers).Map(
               m =>
               {
                   m.ToTable("Authorization");
                   m.MapLeftKey("User");
                   m.MapRightKey("Post");

               });
            //Many to many : Answer
            HasMany(u => u.Answers).WithMany(p => p.Users).Map(
               m =>
               {
                   m.ToTable("AnswerBank");
                   m.MapLeftKey("User");
                   m.MapRightKey("Answer");
                   

               });

            // one to many : event
            HasMany(u => u.Envents).WithRequired(x => x.User).HasForeignKey(m => m.UserId).WillCascadeOnDelete(false);

            HasMany(x=>x.EventComments).WithRequired(x=>x.User).HasForeignKey(x=>x.UserId).WillCascadeOnDelete(false);
            
        }
    }
}
