﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    public class QuestionConfiguration : EntityTypeConfiguration<Question>
    {
        public QuestionConfiguration()
        {
            // one to many : Reponse 
            HasMany(u => u.Answers).WithRequired(x => x.Question).HasForeignKey(m => m.QuestionId).WillCascadeOnDelete(false);
        }
        
    }
}
