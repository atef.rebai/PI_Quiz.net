﻿using MainApplication.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Web;

namespace MainApplication.token
{
    public class JWToken
    {
        public string Token { get; set; }

        public TenantModel getTenantFromToken()
        {
            if (this.Token == null)
                return null;
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadToken(this.Token) as JwtSecurityToken;
            return new TenantModel()
            {
                CustemColor = tokenS.Claims.First(claim => claim.Type == "TenantColor").Value,
                LogoURI = tokenS.Claims.First(claim => claim.Type == "TenantLogoUri").Value,
                Name = tokenS.Claims.First(claim => claim.Type == "TenantName").Value,
                Id= Convert.ToInt32(tokenS.Claims.First(claim => claim.Type == "TenantId").Value)

            };
            
        }


        public UserModel getUserFromToken()
        {
            if (this.Token == null)
                return null;
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadToken(this.Token) as JwtSecurityToken;
            return new UserModel()
            {
                username = tokenS.Claims.First(claim => claim.Type == "unique_name").Value,
                Role = tokenS.Claims.First(claim => claim.Type == "role").Value,
                ProfileImageURL = tokenS.Claims.First(claim => claim.Type == "imageuri").Value,
                Id = Convert.ToInt32(tokenS.Claims.First(claim => claim.Type == "Id").Value)
            };

        }

    }
}