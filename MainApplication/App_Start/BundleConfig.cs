﻿using System.Web;
using System.Web.Optimization;

namespace MainApplication
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap-datepicker.css",
                        "~/Content/bootstrap-timepicker.css",
                        "~/Content/bootstrap-touchspin.css",
                        "~/Content/datatables.css",
                        "~/Content/dropzone.css",
                        "~/Content/fancytree.css",
                        "~/Content/fullcalendar.css",
                        "~/Content/jvectormap.css",
                        "~/Content/morris.css",
                        "~/Content/nestable.css",
                        "~/Content/nouislider.css",
                        "~/Content/style.css",
                        "~/Content/summernote.css",
                        "~/Content/sweetalert.css"));

            bundles.Add(new StyleBundle("~/Content/layout_css").Include(
            "~/Content/Vendor/simplebar.css",
            "~/Content/vendor/material-design-kit.css",
            "~/Content/vendor/sidebar-collapse.min.css",
            "~/Content/style.css",
            "~/Content/morris.css"));


            bundles.Add(new ScriptBundle("~/bundles/layout_scripts").Include(
            "~/Content/vendor/jquery.min.js",
            "~/Content/vendor/popper.min.js",
            "~/Content/vendor/bootstrap.min.js",
            "~/Content/vendor/simplebar.js",
            "~/Content/vendor/dom-factory.js",
            "~/Content/vendor/material-design-kit.js",
            "~/Content/vendor/sidebar-collapse.js",
            "~/Scripts/main.js",
            "~/Scripts/colors.js",
            "~/Content/vendor/raphael.min.js",
            "~/Content/vendor/morris.min.js",
            "~/Content/vendor/moment.min.js",
            "~/Content/vendor/moment-range.min.js"
            ));
        }
    }
}
