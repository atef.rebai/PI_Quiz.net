﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MainApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "adding_comments",
                url: "event/{path_id}/add/comment",
                defaults: new
                {
                    controller = "event",
                    action = "addComment",
                    path_id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
    name: "del_comments",
    url: "event/{path_id}/del/comment",
    defaults: new
    {
        controller = "event",
        action = "delComment",
        path_id = UrlParameter.Optional
    }
);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Auth", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
