﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MainApplication.Startup))]
namespace MainApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
