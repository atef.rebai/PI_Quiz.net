﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainApplication.Models
{
    public class TenantModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoURI { get; set; }
        public string CustemColor { get; set; }
        public string WebSiteURL { get; set; }

        //navigation prop:
        virtual public ICollection<UserModel> Users { get; set; }

    }
}