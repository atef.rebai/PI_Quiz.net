﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainApplication.Models
{
    public class PollModel : PostModel
    {
        public DateTime EndDate { get; set; }
        public string ImageURI { get; set; }
        public bool IsActive
        {
            get { return StartDate < DateTime.Now && EndDate > DateTime.Now; }
        }

    }
}