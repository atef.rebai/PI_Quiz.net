﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainApplication.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public DateTime DatePost { get; set; }
        public string Content { get; set; }
        // cles etrangers 
        public int UserId { get; set; }
        public int TopicId { get; set; }

        //prop de navig
        public virtual TopicModel Topic { get; set; }
        public virtual UserModel User { get; set; }
    }
}