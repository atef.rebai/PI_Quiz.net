﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainApplication.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsMulti { get; set; }
        public float? Point { get; set; }
        public string type { get; set; }
        public string ImageURI { get; set; }
        public string VideoURI { get; set; }
        //cles etrangers
        public int PostId { get; set; }
    }
}