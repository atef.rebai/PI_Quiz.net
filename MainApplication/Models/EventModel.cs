﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainApplication.Models
{
    public class EventModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImageURI { get; set; }
        public string Content { get; set; }
        public DateTime? DateCreation { get; set; }
        public DateTime? EndDate { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public string Location { get; set; }
        public bool IsActive
        {
            get { return DateCreation < DateTime.Now && EndDate > DateTime.Now; }
        }
        // cles etrangers 
        public int UserId { get; set; }
        //prop de navig

        public virtual ICollection<EventCommentModel> EventComments { get; set; }
        public virtual UserModel User { get; set; }



    }
}
