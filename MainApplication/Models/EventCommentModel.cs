﻿using System;

namespace MainApplication.Models
{
    public class EventCommentModel
    {


        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime CreationDate { get; set; }

        //for key
        public int UserId { get; set; }
        public int EventId { get; set; }

        public virtual UserModel User { get; set; }




    }
}