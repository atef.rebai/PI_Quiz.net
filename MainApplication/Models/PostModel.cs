﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainApplication.Models
{
    public class PostModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime StartDate { get; set; }
        // cles etrangers 
        public int PublisherId { get; set; }
        public ICollection<QuestionModel> Questions { get; set; }

    }
}