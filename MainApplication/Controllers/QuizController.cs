﻿using Domain.Entities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using PagedList;
using System.Web;
using System.Web.Mvc;
using MainApplication.token;

namespace MainApplication.Controllers
{
    public class QuizController : Controller
    {
        //HttpClient client;

        //HttpResponseMessage Response;

      

        // GET: Quiz

        [HttpGet]
        public ActionResult Index(string desc)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            //Response = client.GetAsync(string.Format("quiz?desc="+desc)).Result;
            //ViewBag.jiji = Response.Content.ReadAsStringAsync();
            var clientrest = new RestClient("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("quizs?desc=" + desc, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            ViewBag.descrip = desc;


            //     IRestResponse response1 = clientrest.Execute(requestrest);
            //    var content = response1.Content; // raw content as string
            //  ViewBag.jiji = content;
            //   return View();
            requestrest.ToString();
            var response1 = clientrest.Execute<List<Quiz>>(requestrest);

            return View(response1.Data);
        }


        // GET: Quiz/all
        [HttpGet]

        public ActionResult All(int? page)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;


            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("quiz", Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.ToString();
            var response1 = clientrest.Execute<List<Quiz>>(requestrest).Data.ToList();




            int recordsPerPage = 4;
            if (!page.HasValue)
            { page = 1; }





            return View(response1.AsQueryable().OrderByDescending(p => p.Score).ToPagedList(page.Value, recordsPerPage));
        }
        [HttpGet]
        // GET: Quiz/Details/5
        public ActionResult Details(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("quiz/" + id, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.ToString();
            var response1 = clientrest.Execute<Quiz>(requestrest);


            return View(response1.Data);
        }



        // GET: Quiz/Create
        public ActionResult Create()
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };


            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var requestrest = new RestRequest("lesson/", Method.GET);
            requestrest.RequestFormat = DataFormat.Json;
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            var response = clientrest.Execute<List<Lesson>>(requestrest);

            ViewBag.list = response.Data.Select(x =>
                                  new SelectListItem()
                                  {
                                      Text = x.Titre.ToString(),
                                      Value = x.Id.ToString()
                                  });
            return View();
        }

        // POST: Quiz/Create
        [HttpPost]
        public ActionResult Create(Quiz qui)
        {

            try
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;

                var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var requestrest = new RestRequest("quiz", Method.POST);

                requestrest.RequestFormat = DataFormat.Json;
                requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
                requestrest.AddBody(new Quiz
                {

                    Description = qui.Description,
                    Title = qui.Title,
                    Score = qui.Score,
                    //Duration = qui.Duration,
                    StartDate = DateTime.Now,
                    Lesson = qui.Lesson,
                    LessonId = qui.LessonId,
               //     PublisherId = 1

                });

                var response = clientrest.Execute<Quiz>(requestrest);
                return RedirectToAction("All");
            }
            catch
            {
                return View();
            }
        }

        // GET: Quiz/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("quiz/" + id, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.ToString();
            var requestrest1 = new RestRequest("lesson/", Method.GET);
            requestrest.RequestFormat = DataFormat.Json;
            var response1 = clientrest.Execute<Quiz>(requestrest);
            var response = clientrest.Execute<List<Lesson>>(requestrest1);
            ViewBag.list = response.Data.Select(x =>
                                  new SelectListItem()
                                  {
                                      Text = x.Titre.ToString(),
                                      Value = x.Id.ToString()
                                  });
            return View(response1.Data);

        }

        // POST: Quiz/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Quiz qui)
        {
            try
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;

                var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var requestrest = new RestRequest("Quiz/" + id, Method.PUT);

                requestrest.RequestFormat = DataFormat.Json;
                requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
                requestrest.AddBody(new Quiz
                {
                    Id = id,
                    Description = qui.Description,
                    Title = qui.Title,
                    Score = qui.Score,
                    Duration = qui.Duration,
                    StartDate = DateTime.Now,
                    Lesson = qui.Lesson,
                    LessonId = qui.LessonId,
                    PublisherId = 1

            });

                var response = clientrest.Execute<Quiz>(requestrest);


                return RedirectToAction("All");
            }
            catch
            {
                return View();
            }
        }

        // GET: Quiz/Delete/5
        public ActionResult Delete(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("quiz/" + id, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.ToString();
            var response1 = clientrest.Execute<Quiz>(requestrest);
            return View(response1.Data);
        }

        //  POST: Quiz/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;
                Quiz qui = new Quiz();


                var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var requestrest = new RestRequest("quiz/" + id, Method.DELETE);
                requestrest.AddParameter("id", qui.Id = id);
                requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
                IRestResponse response1 = clientrest.Execute(requestrest);
                var content = response1.Content;
                return RedirectToAction("All");
            }
            catch
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult PagingAndSorting(int? page)
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("quiz", Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.ToString();
            var response1 = clientrest.Execute<List<Quiz>>(requestrest).Data.ToList();


            ViewBag.Title = response1;

            int recordsPerPage = 4;
            if (!page.HasValue)
            { page = 1; }





            return View(response1.AsQueryable().OrderByDescending(p => p.Score).ToPagedList(page.Value, recordsPerPage));
        }

    }
}
