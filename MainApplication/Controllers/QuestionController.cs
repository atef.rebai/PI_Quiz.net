﻿using MainApplication.Models;
using MainApplication.token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class QuestionController : Controller
    {
        HttpClient client;
        HttpResponseMessage Response;
        public QuestionController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        // GET: Question
        public ActionResult Index(int idpost)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            Response = client.GetAsync("question?idpost=" +idpost).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            List<QuestionModel> po = JsonConvert.DeserializeObject<List<QuestionModel>>(res);

            return View(po);
        }


        // GET: Question/Details/5
        public ActionResult Details(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            Response = client.GetAsync("question/" + id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            QuestionModel q = JsonConvert.DeserializeObject<QuestionModel>(res);
            return View(q);

        }

        // GET: Question/Create
        public ActionResult Create()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            return View();
        }

        // POST: Question/Create
        [HttpPost]
        public ActionResult Create(QuestionModel qm,int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);


            //return qm.Id+":"+qm.Content + ":" +qm.IsMulti + ":" +qm.type + ":" +qm.PostId;
            try
            {

               
                var obj = JsonConvert.SerializeObject(qm);
                var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                Response = client.PostAsync("question", byteContent).Result;
                // return Response.Content.ReadAsStringAsync().Result;
                // return RedirectToAction("Index","Question",new {@idpost=qm.PostId});
                //return RedirectToRoute("/quest");
                return RedirectToAction("Details","PollMvc", new { @id = id });
            }
            catch
            {
                //return "";
               return View();
            }
        }

        // GET: Question/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            Response = client.GetAsync("question/" + id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            QuestionModel qo = JsonConvert.DeserializeObject<QuestionModel>(res);


            return View(qo);
        }

        // POST: Question/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, QuestionModel qm )
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            try
            {
                var obj = JsonConvert.SerializeObject(qm);
                var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                Response = client.PutAsync("question/" + id, byteContent).Result;

              //   return RedirectToAction("Index", "Question", new { @idpost = qm.PostId });
                // return RedirectToAction("Details", "PollMvc", new { @idp = idp });
                return RedirectToAction("Details", "Question", new { @id = id });
            }
            catch
            {
                return View();
            }
        }

        // GET: Question/Delete/5
        public string Delete(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return "no";
            }

           
            return "ok"+id;
           // return View();
        }

        // POST: Question/Delete/5
        [HttpPost]
        public string Delete(int id, FormCollection collection)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return "no";
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);
            try
            {
                client.DeleteAsync("question/" + id);

                return "ok";
            }
            catch
            {
                return "no";
            }
        }
    }
}
