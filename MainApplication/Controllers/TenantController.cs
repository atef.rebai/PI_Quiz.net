﻿using MainApplication.Models;
using MainApplication.token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class TenantController : Controller
    {
        HttpClient client;
        HttpResponseMessage client_Response;

        public TenantController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: Tenant
        public ActionResult Index()
        {
            return View();
        }

        // GET: Tenant/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Tenant/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tenant/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Tenant/Edit/5
        public ActionResult Edit()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            string res = client.GetStringAsync("Tenant").Result;
            return View(JsonConvert.DeserializeObject<TenantModel>(res));
        }

        // POST: Tenant/Edit/5
        [HttpPost]
        public ActionResult Edit(TenantModel tm)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            var obj = JsonConvert.SerializeObject(tm);
            var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            client_Response= client.PutAsync("Tenant",byteContent).Result;
            return RedirectToAction("logout", "auth");
            //return (client_Response.Content.ReadAsStringAsync().Result);

           
        }

        // GET: Tenant/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Tenant/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
