﻿using Domain.Entities;
using MainApplication.Models;
using MainApplication.token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class PollMvcController : Controller

    {
        HttpClient client;
        HttpResponseMessage Response;
        public PollMvcController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        // GET: PollMvc
        public ActionResult Index(string searchString)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);


            if (String.IsNullOrEmpty(searchString))
            {
                Response = client.GetAsync("poll").Result;
                string res = Response.Content.ReadAsStringAsync().Result;
                List<PollModel> po = JsonConvert.DeserializeObject<List<PollModel>>(res);
                return View(po);
            }
            else { 
                
                Response = client.GetAsync("poll?title="+searchString).Result;
                string res = Response.Content.ReadAsStringAsync().Result;
                List<PollModel> po = JsonConvert.DeserializeObject<List<PollModel>>(res);

                return View(po);

            }



        }

        // GET: PollMvc/Details/5
        public ActionResult Details(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            Response = client.GetAsync("poll/"+id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            PollModel po = JsonConvert.DeserializeObject<PollModel>(res);

            //return po.Questions.ToList()[0].Content;

            return View(po);
        }

        // GET: PollMvc/Create
        public ActionResult Create()
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);



            return View();
        }

        // POST: PollMvc/Create
        [HttpPost]
        public ActionResult Create(PollModel pm, HttpPostedFileBase file)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            if (file == null || file.ContentLength <= 0)
            {
                // return "bad request";
                pm.ImageURI = "https://s3-eu-west-1.amazonaws.com/bucketpi/1.jpg";
                //return RedirectToAction("Create");
            }
            else
            {
                pm.ImageURI = DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                file.SaveAs(Server.MapPath("~/Content/poll_images/") + pm.ImageURI);

            }
            try
            {
                pm.StartDate = DateTime.Now;
                pm.EndDate = DateTime.Now.AddMonths(3);
                


                var obj = JsonConvert.SerializeObject(pm);
                var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                Response = client.PostAsync("poll", byteContent).Result;

                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        // GET: PollMvc/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            Response = client.GetAsync("poll/"+id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            PollModel po = JsonConvert.DeserializeObject<PollModel>(res);


            return View(po);
        }

        // POST: PollMvc/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PollModel pm)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            try
            {
                var obj = JsonConvert.SerializeObject(pm);
                var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                Response = client.PutAsync("poll/"+id, byteContent).Result;

                return RedirectToAction("Index");
                //return Response.Content.ReadAsStringAsync();
            }
            catch
            {
                return View();
               // return null;
            }
        }

        // GET: PollMvc/Delete/5
        public ActionResult Delete(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            return View();
        }

        // POST: PollMvc/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            try
            {
                client.DeleteAsync("poll/" + id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
