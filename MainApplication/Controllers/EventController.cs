﻿using MainApplication.Models;
using MainApplication.token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class EventController : Controller
    {
        HttpClient client;
        HttpResponseMessage client_Response;
        public EventController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: Event
        public  ActionResult Index(string search)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }
               
            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username; 
            ViewBag.userid = token.getUserFromToken().Id;
            
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            if (search == null || search.Equals(""))
            {
                
            string res = client.GetStringAsync("Event").Result;
            return View(JsonConvert.DeserializeObject<List<EventModel>>(res));
            }
            else { 
                string res = client.GetStringAsync("Event?search=" + search).Result;
            return View(JsonConvert.DeserializeObject<List<EventModel>>(res));
            }
        }

        // GET: Event/Details/5
        public ActionResult Details(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);


            client_Response = client.GetAsync("event/" + id).Result;
            
            string e = client_Response.Content.ReadAsStringAsync().Result;
            EventModel model=JsonConvert.DeserializeObject<EventModel>(e);
           
            return View(model);
        }

        // GET: Event/Create
        public ActionResult Create()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;

            return View();
        }

        // POST: Event/Create
        [HttpPost]
        public ActionResult Create(EventModel ev, HttpPostedFileBase file)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            if (file == null || file.ContentLength <= 0)
            {
                // return "bad request";
                ev.ImageURI = "1.jpg";
            }
            else
            {
                Stream st = file.InputStream;
               
                //file.SaveAs(Server.MapPath("~/Content/uploaded_event_images/") + ev.ImageURI);

                

                string name = DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                ev.ImageURI = "https://s3-eu-west-1.amazonaws.com/bucketpi/" + name;
                string myBucketName = "bucketpi"; //your s3 bucket name goes here  
                string s3DirectoryName = "";
                string s3FileName = @name;
                bool a;
                AmazonUploader myUploader = new AmazonUploader();
                a = myUploader.sendMyFileToS3(st, myBucketName, s3DirectoryName, s3FileName);
        }

            try
            {
                ev.DateCreation = DateTime.Now;
               
                

                var obj = JsonConvert.SerializeObject(ev);
                var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                client_Response = client.PostAsync("event", byteContent).Result;
                
              
                 EventModel m=JsonConvert.DeserializeObject<EventModel>(client_Response.Content.ReadAsStringAsync().Result);
                return RedirectToAction("Details", new  { @Id=m.Id});
            }
            catch
            {
                   return View();
                //return "catch exception";
            }
            
            
           // return JsonConvert.SerializeObject(ev);
           
        }

        // GET: Event/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);
            client_Response = client.GetAsync("event/" + id).Result;
            string e = client_Response.Content.ReadAsStringAsync().Result;
            EventModel model = JsonConvert.DeserializeObject<EventModel>(e);

            return View(model);


           
        }

        // POST: Event/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EventModel ev)
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;


            client = new HttpClient();
             client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
             
             client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);
            
            var obj = JsonConvert.SerializeObject(ev);
            var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            client_Response = client.PutAsync("event/"+id, byteContent).Result;
             return RedirectToAction("details",new { @id=id});
        }

        // GET: Event/Delete/5
        public ActionResult Delete(int id)
        {


            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;




            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);
            client_Response= client.DeleteAsync("Event/"+id).Result;
            return RedirectToAction("index");
           
        }

        // POST: Event/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        // POST: event/{id}/add/comment
        [HttpPost]
        public string addComment(int path_id, EventCommentModel com)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return null;
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            com.CreationDate = DateTime.Now;
            com.EventId = path_id;
            
            var obj = JsonConvert.SerializeObject(com);
            //return obj;
            var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            client_Response = client.PostAsync("Event/"+path_id+"/comment", byteContent).Result;

            
            return client_Response.Content.ReadAsStringAsync().Result;
        }
        // GET: event/{id}/del/comment
        [HttpPost]
        public string delComment(int path_id)
        {
            return "delcomment:"+ path_id;
        }


    }
}
