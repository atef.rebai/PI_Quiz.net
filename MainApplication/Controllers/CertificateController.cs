﻿using Domain.Entities;
using MainApplication.token;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class CertificateController : Controller
    {
        // GET: Certificate
        public ActionResult Index()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;

            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("certificate", Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

            requestrest.ToString();
            var response1 = clientrest.Execute<List<Certificate>>(requestrest);
            return View(response1.Data);
        }
        [HttpGet]
        // GET: Certificate/Details/5
        public ActionResult Details(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("Certificate/"+id , Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

            int idusr = clientrest.Execute<Certificate>(requestrest).Data.UserId;
            var requestrest1 = new RestRequest("user/" + idusr , Method.GET);
            requestrest1.AddHeader("content-type", "application/json");
            requestrest1.AddHeader("Authorization", "Bearer " + Session["token"]);

            int idlsn = clientrest.Execute<Certificate>(requestrest).Data.LessonId;
            var requestrest2 = new RestRequest("lesson/" + idlsn, Method.GET);
            requestrest2.AddHeader("content-type", "application/json");
            requestrest2.AddHeader("Authorization", "Bearer " + Session["token"]);

            ViewBag.usrName =  clientrest.Execute<User>(requestrest1).Data.Name;
            ViewBag.usLastrName = clientrest.Execute<User>(requestrest1).Data.LastName;
            ViewBag.lsnTitle = clientrest.Execute<Lesson>(requestrest2).Data.Titre;
            requestrest.ToString();
            var response1 = clientrest.Execute<Certificate>(requestrest);
            return View(response1.Data);
        }

        // GET: Certificate/Create
        public ActionResult Create()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("lesson/", Method.GET);
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.RequestFormat = DataFormat.Json;
            var response = clientrest.Execute<List<Lesson>>(requestrest);
            ViewBag.list = response.Data.Select(x =>
                   new SelectListItem()
                   {
                       Text = x.Titre.ToString(),
                       Value = x.Id.ToString()
                   });

            
            return View();
        }

        // POST: Certificate/Create
        [HttpPost]
        public ActionResult Create(Certificate cert)
        {
            try
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;
                var client = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var request = new RestRequest("Certificate", Method.POST);
                request.AddHeader("Authorization", "Bearer " + Session["token"]);

                request.RequestFormat = DataFormat.Json;
                request.AddBody(new Certificate
                {
                    Titre = cert.Titre,
                    Date = DateTime.Now,
                    LessonId = cert.LessonId,
                    UserId= cert.UserId,
                    

                }
                    );
                client.Execute<Certificate>(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Certificate/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("Certificate/" + id, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            // add token to the header
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

            var requestrest1 = new RestRequest("lesson/", Method.GET);
            requestrest.RequestFormat = DataFormat.Json;
            var response = clientrest.Execute<List<Lesson>>(requestrest1);
            ViewBag.list = response.Data.Select(x =>
                   new SelectListItem()
                   {
                       Text = x.Titre.ToString(),
                       Value = x.Id.ToString()
                   });



            requestrest.ToString();
            var response1 = clientrest.Execute<Certificate>(requestrest);
            return View(response1.Data);
        }

        // POST: Certificate/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Certificate cert)
        {
            try
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;
                var client = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var request = new RestRequest("Certificate/" + id, Method.PUT);
                request.AddHeader("Authorization", "Bearer " + Session["token"]);

                request.RequestFormat = DataFormat.Json;
                request.AddBody(new Certificate
                {
                   Id = id,
                   Titre = cert.Titre,
                   LessonId = cert.LessonId,
                   UserId=cert.UserId,
                   Date=DateTime.Now 

                });

                var response = client.Execute<Certificate>(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Certificate/Delete/5
        public ActionResult Delete(int id)
        {
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;
                var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var requestrest = new RestRequest("Certificate/" + id, Method.GET);
                requestrest.AddHeader("content-type", "application/json");
                requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

                requestrest.ToString();
                var response1 = clientrest.Execute<Certificate>(requestrest);
                return View(response1.Data);
            };
        }

        // POST: Certificate/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;

                Certificate cert = new Certificate();
                var client = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var request = new RestRequest("certificate/" + id, Method.DELETE);
                request.AddHeader("Authorization", "Bearer " + Session["token"]);

                request.AddParameter("id", cert.Id = id);
                IRestResponse response1 = client.Execute(request);
                var content = response1.Content;
                return RedirectToAction("Index");

                
            }
            catch
            {
                return View();
            }
        }
       
    }


}
