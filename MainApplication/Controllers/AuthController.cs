﻿using MainApplication.Models;
using MainApplication.token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class AuthController : Controller
    {
        HttpClient client;
        HttpResponseMessage client_Response;
        public AuthController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        // GET: Auth
        public ActionResult Index()
        {
            if (TempData["err_auth_msg"] != null)
                this.ViewBag.err_auth_msg = TempData["err_auth_msg"];
            return View();
        }



        // POST: Auth/Create
        [HttpPost]
        public ActionResult auth(UserModel user)
        {
            
            var obj = JsonConvert.SerializeObject(user);
            var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            client_Response = client.PostAsync("auth", byteContent).Result;
            JWToken token = JsonConvert.DeserializeObject<JWToken>(client_Response.Content.ReadAsStringAsync().Result);
            if (String.IsNullOrEmpty(token.Token))
            {
                TempData["err_auth_msg"] = "username or password invalid";
                return RedirectToAction("index");
            }
            this.Session["token"] = token.Token;
            return RedirectToAction("All","quiz");
        }

        public string getToken()
        {
            return "token:"+this.Session["token"];
        }

        [HttpGet]
        public ActionResult logout()
        {
            this.Session["token"] = null;
            return RedirectToAction("index");

        }
        
    }
}
