﻿using Domain.Entities;
using MainApplication.token;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class AnswerQuizController : Controller
    {
        // GET: Answer
        public ActionResult Index()
        {
            return View();
        }

        // GET: Answer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Answer/Create
        public ActionResult Create()
        {

            //var clientrest1 = new RestClient("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api");
            //var requestrest1 = new RestRequest("AnswerQuiz/AnsByQues?idQuestion =" + idQ, Method.GET);
            //requestrest1.AddHeader("content-type", "application/json");
            //idQ = clientrest1.Execute<Answer>(requestrest1).Data.Id;
            return View();

        }

        // POST: Answer/Create
        [HttpPost]
        public ActionResult Create(Answer ans, HttpPostedFileBase File)
        {
            try
            {
                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;

                var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var requestrest = new RestRequest("answer/", Method.POST);

                requestrest.RequestFormat = DataFormat.Json;
                requestrest.AddHeader("Content-Type", "multipart/form-data");
                requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

                requestrest.AddBody(new Answer
                {
                    Content = ans.Content,
                    IsCorrect = ans.IsCorrect,
                    type = ans.type,
                    ImageURI = File.FileName,
                    QuestionId = Convert.ToInt32(Request.QueryString["idQuestion"])

                });




                var path = Path.Combine(Server.MapPath("C://Users/Jihene/Desktop/exam proba mai 17/"), File.FileName);
                File.SaveAs(path);
                var response = clientrest.Execute<Answer>(requestrest);

                return RedirectToAction("AnsByQues");


                //return CreatedAtAction("DefaultApi", new { controller = "AnswerQuizController", id = ans.Id }, ans); ;



            }
            catch
            {
                return View();
            }
        }

        // GET: Answer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Answer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Answer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Answer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [HttpGet]
        // GET: AnswerQuiz/AnsByQues/5
        public ActionResult AnsByQues(int? idQuestion)
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;

            var clientrest = new RestClient("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("AnswerQuiz?idQuestion=" + idQuestion, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

            requestrest.ToString();
            var response1 = clientrest.Execute<List<Answer>>(requestrest);


            var req = new RestRequest("QuestionQuiz/" + idQuestion, Method.GET);
            req.AddHeader("content-type", "application/json");
            req.AddHeader("Authorization", "Bearer " + Session["token"]);
            ViewBag.Qcentent = clientrest.Execute<Question>(req).Data.Content;
            ViewBag.idQ = idQuestion;

            return View(response1.Data);



        }
    }
}
