﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using MainApplication.token;

namespace MainApplication.Controllers
{
    public class LessonController : Controller
    {

        // GET: Lesson
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("lesson", Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.ToString();
            var response1 = clientrest.Execute<List<Lesson>>(requestrest);

            return View(response1.Data);
         
        }

        // GET: Lesson/Details/5
        [HttpGet]
        public ActionResult Details(int id)
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("lesson/"+id, Method.GET);
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.AddHeader("content-type", "application/json");

            requestrest.ToString();
            var response1 = clientrest.Execute<Lesson>(requestrest);
            return View(response1.Data);
        }

        // GET: Lesson/Create
        public ActionResult Create()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            return View();
        }

        // POST: Lesson/Create
        [HttpPost]
        public ActionResult Create(Lesson less)
        {
            try
            {

                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;
                var client = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var request = new RestRequest("lesson", Method.POST);
                request.AddHeader("Authorization", "Bearer " + Session["token"]);
                request.RequestFormat = DataFormat.Json;

                request.AddBody(new Lesson
                {
                    Titre = less.Titre,
                    URIImage = less.URIImage,
                    URIVideo = less.URIVideo,
                    
                }
                    );
                client.Execute<Lesson>(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Lesson/Edit/5
        public ActionResult Edit(int id)
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var requestrest = new RestRequest("lesson/" + id, Method.GET);
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.AddHeader("content-type", "application/json");
                requestrest.ToString();
                var response1 = clientrest.Execute<Lesson>(requestrest);
                return View(response1.Data);
                
        }

        // POST: Lesson/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Lesson less)
        {
            try
            {

                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;
                var client = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var request = new RestRequest("Lesson/"+id, Method.PUT);
                request.AddHeader("Authorization", "Bearer " + Session["token"]);

                request.RequestFormat = DataFormat.Json;
                request.AddBody(new Lesson
                {
                    Id = id,
                    Titre = less.Titre,
                    URIImage = less.URIImage,
                    URIVideo = less.URIVideo

                });

                var response = client.Execute<Lesson>(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Lesson/Delete/5
        public ActionResult Delete(int id)
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            var requestrest = new RestRequest("lesson/" + id, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);
            requestrest.ToString();
            var response1 = clientrest.Execute<Lesson>(requestrest);
            return View(response1.Data);
        }

        // POST: Lesson/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {

                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;
                Lesson less = new Lesson();
                var client = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var request = new RestRequest("lesson/"+id, Method.DELETE);
                request.AddHeader("Authorization", "Bearer " + Session["token"]);

                request.AddParameter("id", less.Id = id);
                IRestResponse response1 = client.Execute(request);
                var content = response1.Content;
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult search (string titre)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            //var client = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            //var request = new RestRequest("lesson/" + id, Method.GET);


            return View();

        }
    }
}
