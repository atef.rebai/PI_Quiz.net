﻿using Domain.Entities;
using MainApplication.token;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class QuestionQuizController : Controller
    {
        // GET: QuestionQuiz
        public ActionResult Index()
        {
            return View();
        }

        // GET: QuestionQuiz/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: QuestionQuiz/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuestionQuiz/Create
        [HttpPost]
        public ActionResult Create(Question ques, int id)
        {

            try
            {

                if (Session["token"] == null)
                {
                    TempData["err_auth_msg"] = "you are not authenticated to access that page";
                    return RedirectToAction("index", "Auth");
                }

                JWToken token = new JWToken() { Token = Session["token"].ToString() };
                ViewBag.tenantname = token.getTenantFromToken().Name;
                ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
                ViewBag.tenantid = token.getTenantFromToken().Id;

                ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
                ViewBag.username = token.getUserFromToken().username;
                ViewBag.userid = token.getUserFromToken().Id;

                var clientrest = new RestClient("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                var requestrest = new RestRequest("question" + id, Method.POST);
                requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

                requestrest.RequestFormat = DataFormat.Json;
                requestrest.AddBody(new Question
                {
                    Content = ques.Content,
                    IsMulti = false,
                    PostId = id,
                    //Answers = new List<Answer>
                    //{

                    //      new Answer
                    //    {
                    //          Content = "hahaha",
                    //          IsCorrect = true  ,
                    //          QuestionId = ques.Id
                    //    }
                    //}


                });
                var response = clientrest.Execute<Question>(requestrest);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuestionQuiz/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: QuestionQuiz/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuestionQuiz/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: QuestionQuiz/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [HttpGet]
        // GET: QuestionQuiz/QuesByQuiz/5
        public ActionResult QuesByQuiz(int? idQuiz)
        {

            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.tenantid = token.getTenantFromToken().Id;

            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            var clientrest = new RestClient("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api");
            var requestrest = new RestRequest("QuestionQuiz?idQuiz=" + idQuiz, Method.GET);
            requestrest.AddHeader("content-type", "application/json");
            requestrest.AddHeader("Authorization", "Bearer " + Session["token"]);

            requestrest.ToString();
            var response1 = clientrest.Execute<List<Question>>(requestrest);
            ViewBag.nbr = response1.Data.Count();
            return View(response1.Data);



        }




    }
}
