﻿using MainApplication.Models;
using MainApplication.token;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class TopicController : Controller
    {
        HttpClient client;
        HttpResponseMessage Response;
        public TopicController()
        {
           

            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: Topic
        public ActionResult Index(string searchString)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);



            if (string.IsNullOrEmpty(searchString) )
            {
                
                Response = client.GetAsync("topic").Result;
                string res = Response.Content.ReadAsStringAsync().Result;
                List<TopicModel> tm = JsonConvert.DeserializeObject<List<TopicModel>>(res);
                return View(tm);
                
            }
            else
            {
                
                Response = client.GetAsync("topic?topic=" + searchString).Result;
                string res = Response.Content.ReadAsStringAsync().Result;
                List<TopicModel> tm = JsonConvert.DeserializeObject<List<TopicModel>>(res);
                
                 return View(tm);

            }


        }


        // GET: Topic/Details/5
        public ActionResult Details(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            Response = client.GetAsync("topic/" + id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            TopicModel tm = JsonConvert.DeserializeObject<TopicModel>(res);

            //return po.Questions.ToList()[0].Content;

            return View(tm);
        }

        // GET: Topic/Create
        public ActionResult Create()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            return View();
        }

        // POST: Topic/Create
        [HttpPost]
        public ActionResult Create(TopicModel tm, HttpPostedFileBase file)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);


            if (file == null || file.ContentLength <= 0)
            {
                // return "bad request";
                return RedirectToAction("Create");
            }

            
                tm.CreatedOn = DateTime.Now;
                tm.ImageURI = DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                file.SaveAs(Server.MapPath("~/Content/uploaded_topic_images/") + tm.ImageURI);


                var obj = JsonConvert.SerializeObject(tm);
            var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            Response = client.PostAsync("topic", byteContent).Result;

            return RedirectToAction("Index");
        }

        // GET: Topic/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            Response = client.GetAsync("topic/" + id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            TopicModel tm = JsonConvert.DeserializeObject<TopicModel>(res);


            return View(tm);
        }

        // POST: Topic/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, TopicModel tm)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);
            try
            {
                var obj = JsonConvert.SerializeObject(tm);
                var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                Response = client.PutAsync("topic/" + id, byteContent).Result;

                return RedirectToAction("Index");
                //return Response.Content.ReadAsStringAsync();
                //return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Topic/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Topic/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            JWToken token = new JWToken() { Token = Session["token"].ToString() };
            ViewBag.tenantname = token.getTenantFromToken().Name;
            ViewBag.tenantcolor = token.getTenantFromToken().CustemColor;
            ViewBag.userimage = token.getUserFromToken().ProfileImageURL;
            ViewBag.username = token.getUserFromToken().username;
            ViewBag.userid = token.getUserFromToken().Id;
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            try
            {
                client.DeleteAsync("topic/" + id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        /* public ActionResult search(string topic)
         {
             Response = client.GetAsync("topic?topic=" + topic).Result;
             string res = Response.Content.ReadAsStringAsync().Result;
             TopicModel tm = JsonConvert.DeserializeObject<TopicModel>(res);
             return View(tm);

         }*/
       
    }

}
