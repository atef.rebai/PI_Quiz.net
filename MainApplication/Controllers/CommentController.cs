﻿using MainApplication.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MainApplication.Controllers
{
    public class CommentController : Controller
    {

        HttpClient client;
        HttpResponseMessage Response;
        public CommentController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-service-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: Comment
        public ActionResult Index()
        {
            if (Session["token"] == null)
            {
                TempData["err_auth_msg"] = "you are not authenticated to access that page";
                return RedirectToAction("index", "Auth");
            }

            Response = client.GetAsync("comment").Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            List<CommentModel> cm = JsonConvert.DeserializeObject<List<CommentModel>>(res);

            return View(cm);
        }

        // GET: Comment/Details/5
        public ActionResult Details(int id)
        {
            Response = client.GetAsync("comment/" + id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            CommentModel cm = JsonConvert.DeserializeObject<CommentModel>(res);
            
            return View(cm);
        }

        // GET: Comment/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Comment/Create
        [HttpPost]
        public ActionResult Create(CommentModel cm)
        {
            var obj = JsonConvert.SerializeObject(cm);
            var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            Response = client.PostAsync("comment", byteContent).Result;

            return RedirectToAction("Index");
        }

        // GET: Comment/Edit/5
        public ActionResult Edit(int id)
        {
            Response = client.GetAsync("comment/" + id).Result;
            string res = Response.Content.ReadAsStringAsync().Result;
            CommentModel cm = JsonConvert.DeserializeObject<CommentModel>(res);


            return View(cm);
        }

        // POST: Comment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CommentModel cm)
        {
            try
            {
                var obj = JsonConvert.SerializeObject(cm);
                var buffer = System.Text.Encoding.UTF8.GetBytes(obj);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                Response = client.PutAsync("comment/" + id, byteContent).Result;

                return RedirectToAction("Index"); 
            }
            catch
            {
                return View();
            }
        }

        // GET: Comment/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Comment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                client.DeleteAsync("comment/" + id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
