﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class Topic
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ImageURI { get; set; }

        // cle etrangeres 
        public int UserId { get; set; }
         //prop de navig

        virtual public User Publisher { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
