﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Poll : Post
    {
        public DateTime EndDate { get; set; }
        public string ImageURI { get; set; }
        public bool IsActive
        {
            get { return StartDate < DateTime.Now && EndDate > DateTime.Now; }
        }
        
        //prop de navig
        public virtual Event Event { get; set; }

    }
}
