﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class EventComment
    {
        public int Id { get; set; }
        public String Content { get; set; }
        public DateTime CreationDate { get; set; }

        //for key
        public int UserId { get; set; }
        public int EventId { get; set; }

        //nav prop
        public virtual Event Event { get; set; }
        public virtual User User { get; set; }
    }
}
