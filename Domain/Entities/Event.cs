﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class Event
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string  ImageURI { get; set; }
        public string Content { get; set; }
        public DateTime? DateCreation { get; set; }
        public DateTime? EndDate { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public string Location { get; set; }
        public int NbPlaces { get; set; }
        public bool IsActive
        {
            get { return DateCreation < DateTime.Now && EndDate > DateTime.Now; }
        }
        // cles etrangers 
        public int UserId { get; set; }
        //prop de navig
        public virtual User User { get; set; }
        public virtual Poll Poll { get; set; }
        public virtual ICollection<EventComment> EventComments { get; set; }
        public ICollection<EventParticipations> ParticipatingUsers { get; set; }



    }
}
