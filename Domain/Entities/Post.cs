﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
      
        public DateTime StartDate { get; set; }
        // cles etrangers 
        public int PublisherId { get; set; }

        //navigation prop




        virtual public User Publisher { get; set; }
        virtual public ICollection<User> AuthorizedUsers { get; set; }
        virtual public ICollection<Question> Questions { get; set; }

    }
}
