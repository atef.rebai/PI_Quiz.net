﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
 public   class Lesson

    {
        public int Id { get; set; }
        public string Titre { get; set; }
        public string URIImage { get; set; }
        public string URIVideo { get; set; }

        //


        //nav prop  
        virtual public ICollection<Quiz> Quizs { get; set; }
        public virtual ICollection<Certificate> Certificates { get; set; }
    }
}
