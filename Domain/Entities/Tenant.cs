﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Tenant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoURI { get; set; }
        public string CustemColor { get; set; }
        public string WebSiteURL { get; set; }

        //navigation prop:
        virtual public ICollection<User> Users { get; set; }

    }
}
