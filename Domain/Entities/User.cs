﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class User
    {

        
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string username { get; set; }
        public string Password { get; set; }
        public string ProfileImageURL { get; set; }
        public string State { get; set; }
        public string Role { get; set; }
        public string Sexe { get; set; }
        // cle etrangers 
        public int TenantId { get; set; }


        //navigation prop
        [ForeignKey("TenantId")]
        virtual public Tenant Tenant { get; set; }
        virtual public ICollection<Post> CreatedPosts { get; set; }
        public ICollection<Post> AuthorizedPosts { get; set; }
        public virtual ICollection<Topic> Topics { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Event>Envents { get; set; }
        public virtual ICollection<EventComment> EventComments { get; set; }
        public virtual ICollection<EventParticipations> EventParticipations { get; set; }
        public virtual ICollection<Certificate> Certificates { get; set; }
    }
}
