﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
  public  class Quiz : Post
    {

        public TimeSpan? Duration { get; set; }
        public int Score { get; set; }



        //prop nav
        public virtual Lesson Lesson { get; set; }
       

        //for key
        public int? LessonId { get; set; }

    }
}
