﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class EventParticipations
    {
       
        public DateTime CreationDate { get; set; }
        public string State { get; set; }
        public string ActivationKey { get; set; }

        //keys
        public int UserId { get; set; }
        public int EventId { get; set; }
        //nav prop
        public  User User { get; set; }
        public virtual Event Event { get; set; }

    }
}
