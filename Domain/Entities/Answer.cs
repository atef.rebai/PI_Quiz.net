﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Answer
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsCorrect { get; set; }
        public string type { get; set; }
        public string ImageURI { get; set; }
        //foreign key
        public int QuestionId { get; set; }


        //navigation prop
        virtual public Question Question { get; set; }
        public virtual ICollection<User> Users { get; set; }

    }
}
