﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
  public  class Comment
    {
        public int Id { get; set; }
        public DateTime DatePost { get; set; }
        public string Content { get; set; }
        // cles etrangers 
        public int UserId { get; set; }
        public int TopicId { get; set; }

        //prop de navig
        public virtual Topic Topic { get; set; }
        public virtual User User { get; set; }
    }
}
