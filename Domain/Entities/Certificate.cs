﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class Certificate
    {
        public int Id { get; set; }
        public string Titre { get; set; }
        public DateTime Date { get; set; }


        //for key

        public int UserId { get; set; }
        public int LessonId { get; set; }



        //prop nav
        public virtual User User  { get; set; }
        public virtual Lesson  Lesson { get; set; }
    }
}
