﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class EventController : ApiController
    {
        IEventService s;
            public EventController()
        {
            s = new EventService();
        }

        public List<Event> Get()
        {
           
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.getMany().ToList<Event>();
        }

        
        // GET: api/Event
        public List<Event> Get(string search)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            if (search.Equals(""))
            return s.getMany().ToList<Event>();
            return s.getByLocationOrName(search).ToList();
        }

    
        // GET: api/Event/5
        public Event Get(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.GetById(id);
        }

        // POST: api/Event
        public Event Post(Event arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Add(arg);
        }

        // PUT: api/Event/5
        public Event Put(int id, Event arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Update(id, arg);
        }

        // DELETE: api/Event/5
        public void Delete(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            s.Delete(id);
        }

        // POST api/Event/{id}/comment
        public EventComment AddComment(int id, EventComment comment)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            
            return s.AddCommentToEvent(comment,id);
        }
    }
}
