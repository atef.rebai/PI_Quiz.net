﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class CertificateSController : ApiController

    {
        ICertificateService<Certificate> c = new CertificateService();
        // GET: api/CertificateS
        public IEnumerable<Certificate> Get(string title)
        {
            c.TokenHeader =  HttpContext.Current.Request.Headers["Authorization"];
            return c.getCertificationByTitle(title).ToList();
        }

        // GET: api/CertificateS/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/CertificateS
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/CertificateS/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CertificateS/5
        public void Delete(int id)
        {
        }
    }
}
