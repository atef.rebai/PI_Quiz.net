﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class CommentController : ApiController
    {
        ICommentService<Comment> s = new CommentService<Comment>();
        // GET: api/Comment
        public List<Comment> Get()
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.getMany().ToList<Comment>();
        }

        // GET: api/Comment/5
        public Comment Get(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.GetById(id);
        }

        // POST: api/Comment
        public Comment Post(Comment arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Add(arg);
        }

        // PUT: api/Comment/5
        public Comment Put(int id, Comment arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Update(id, arg);
        }

        // DELETE: api/Comment/5
        public void Delete(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            s.Delete(id);
        }
    }
}
