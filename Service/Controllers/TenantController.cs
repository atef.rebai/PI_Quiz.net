﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class TenantController : ApiController
    {
        ITenantService t = new TenantService();
        // GET: api/Tenant
        public Tenant Get()
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return t.Get();
        }

      

        // PUT: api/Tenant/5
        public Tenant Put(Tenant arg)
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return t.Update(arg);
        }

        // DELETE: api/Tenant/5
        public void Delete(int id)
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            t.Delete(id);
        }
    }
}
