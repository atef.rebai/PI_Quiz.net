﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class UserController : ApiController
    {
       IUserService<User> t = new UserService<User>();
        // GET: api/User
        public List<User> Get()
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return t.getMany().ToList<User>();
        }

        // GET: api/User/5
        public User Get(int id)
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return t.GetById(id);
        }

        // POST: api/User
        public User Post(User arg)
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return t.Add(arg);
        }

        // PUT: api/User/5
        public User Put(int id, User arg)
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return t.Update(id, arg);
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
            t.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            t.Delete(id);
        }
    }
}
