﻿using Domain.Entities;
using Domain.token;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Service.Controllers
{
    public class AuthController : ApiController
    {
        IAuthService authservice = new AuthService();
        public JWToken Post(User user)
        {
            return authservice.Auth(user);
        }
    }
}
