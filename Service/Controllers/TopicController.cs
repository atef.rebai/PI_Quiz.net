﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class TopicController : ApiController
    {
        ITopicService<Topic> s = new TopicService();

        [HttpGet]
        public IEnumerable<Topic> Search(string topic)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Search(topic).ToList();
        }


        // GET: api/Topic
        public List<Topic> Get()
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.getMany().ToList<Topic>();
        }

        // GET: api/Topic/5
        public Topic Get(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.GetById(id);
        }

        // POST: api/Topic
        public Topic Post(Topic arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Add(arg);
        }

        // PUT: api/Topic/5
        public Topic Put(int id, Topic arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Update(id, arg);
        }

        // DELETE: api/Topic/5
        public void Delete(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            s.Delete(id);
        }
    }
}
