﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace Service.Controllers
{
    public class LessonSController : ApiController
    {

        ILessonService<Lesson> l = new LessonService();
        // GET: api/LessonS
        public IEnumerable<Lesson> Get(string titre)
        {
            l.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return l.getLessonByTitre(titre).ToList();
        }

        // GET: api/LessonS/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LessonS
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/LessonS/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LessonS/5
        public void Delete(int id)
        {
        }
    }
}
