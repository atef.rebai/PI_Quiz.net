﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class AnswerQuizController : ApiController
    {
        IAnswerQuizService<Answer> a = new AnswerQuizService();

        // GET: api/AnswerQuiz
        public List<Answer> Get()
        { 
            a.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return a.getMany().ToList<Answer>();
        }
        [HttpGet]
        public IEnumerable<Answer> GetAnsByQuestion(int idQuestion)
        {
            a.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return a.getAnsByQuestion(idQuestion).ToList();

        }

        // GET: api/AnswerQuiz/5
        public Answer Get(int id)
        {
            a.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return a.GetById(id);
        }

        // POST: api/AnswerQuiz
        public Answer Post(Answer arg)
        {
            a.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return a.Add(arg);
        }

        // PUT: api/AnswerQuiz/5
        public Answer Put(int id, Answer arg)
        {
            a.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return a.Update(id, arg);
        }

        // DELETE: api/AnswerQuiz/5
        public void Delete(int id)
        {
            a.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            a.Delete(id);
        }
    }
}
