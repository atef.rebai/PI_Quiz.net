﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;


namespace Service.Controllers
{
    public class QuizsController : ApiController
    {
        IQuizService<Quiz> q = new QuizService();

        // GET: api/Quiz?Desc=desc
        public IEnumerable<Quiz> Get(string desc)
        { 
            q.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return q.getQuizByDescription(desc).ToList();
        }



        //public IEnumerable<Quiz> Get(string desc)
        //{
        //    return q.getQuizByDescription(desc).ToList();
        //}

        // GET: api/Quiz/5
        public string Get(int id)
        {
            return "value";
        }

        //public System.Web.Helpers.Chart Chart()
        //{
        //    return q.Chart();
        //}
        // POST: api/Quiz
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Quiz/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Quiz/5
        public void Delete(int id)
        {
        }
    }
}
