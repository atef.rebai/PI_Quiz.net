﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class QuestionController : ApiController
    {
        IQuestion<Question> s = new QuestionService();
        

        // GET: api/Question
        public IEnumerable<Question> Getbypoll(int idpost)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.getQuestionByPoll(idpost).ToList();
        }

        // GET: api/Question
        public IEnumerable<Question> Get()
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.getMany().ToList<Question>();
        }

        // GET: api/Question/5
        public Question Get(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.GetById(id);
        }

        // POST: api/Question
        public Question Post(Question arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Add(arg);
        }

        // PUT: api/Question/5
        public Question Put(int id, Question arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Update(id, arg);
        }

        // DELETE: api/Question/5
        public void Delete(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            s.Delete(id);
        }


        // POST api/Poll/{id}/question
        public Question AddQuestion(int id, Question question)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.AddQuestionToPoll(question, id);
        }

    }
}
