﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class PollController : ApiController
    {
        IPoll<Poll> s = new PollService();
        [HttpGet]
        // GET: api/Question
        public IEnumerable<Poll> Getbytitle(string title)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.getPollByTitle(title).ToList();
        }
        // GET: api/Poll
        public IEnumerable<Poll> Get()
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.getMany().ToList<Poll>();
        }

        // GET: api/Poll/5
        public Poll Get(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.GetById(id);
        }

        // POST: api/Poll
        public Poll Post(Poll arg)
        {

            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Add(arg);
        }

        // PUT: api/Poll/5
        public Poll Put(int id, Poll arg)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return s.Update(id, arg);
        }

        // DELETE: api/Poll/5
        public void Delete(int id)
        {
            s.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            s.Delete(id);
        }
    }
}
