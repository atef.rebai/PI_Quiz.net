﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Service.Controllers
{
    public class QuestionQuizController : ApiController
    {
        IQuestionQuizService<Question> q = new QuestionQuizService();
        //    IQuizService<Quiz> q = new QuizService();

        public int nbrQuestionByQuiz(int idqz)
        {
            q.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return q.nbrQuestionByQuiz(idqz);
        }



        // GET: api/QuestionQuiz

        public IEnumerable<Question> GetByQ(int idQuiz)
        {
            q.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return q.getQuisByQuiz(idQuiz).ToList();

        }

        // GET: api/QuestionQuiz/5
        public Question Get(int id)
        {
            q.TokenHeader = HttpContext.Current.Request.Headers["Authorization"];
            return q.GetById(id);
        }

        // POST: api/QuestionQuiz
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/QuestionQuiz/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/QuestionQuiz/5
        public void Delete(int id)
        {
        }
    }
}
