﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class AnswerQuizService : IAnswerQuizService<Answer>
    {

        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }
        public AnswerQuizService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public Answer Add(Answer arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<Answer>("answer", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Answer>().Result;
        }



        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("answer/" + arg);
        }

        public Answer GetById(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("answer/" + arg).Result;
            Answer ans = Response.Content.ReadAsAsync<Answer>().Result;

            return ans;
        }

        public IEnumerable<Answer> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("answer").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Answer>>().Result;
        }

        public Answer Update(int id, Answer arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PutAsJsonAsync<Answer>("answer/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Answer>().Result;
        }

        IEnumerable<Answer> IAnswerQuizService<Answer>.getAnsByQuestion(int idQuestion)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            return getMany().Where(ques => ques.QuestionId.Equals(idQuestion)).ToList();

        }

        //public string GetQuestionCententForAns(int idSouel)
        //{
        //   return GetById()
        //}


        public Answer CreateAByQues(Answer an, int idQuest)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            throw new NotImplementedException();
        }



    }


}