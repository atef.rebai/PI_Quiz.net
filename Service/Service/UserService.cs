﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class UserService<Tenant> : IUserService<User>
    {
        
            HttpClient client;
            HttpResponseMessage Response;
        public string TokenHeader { get; set; }


        public UserService()
            {
                client = new HttpClient();
                client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            }
            public User Add(User arg)
            {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<User>("user", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
                return Response.Content.ReadAsAsync<User>().Result;
            }

            public void Delete(int arg)
            {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("user/" + arg);
            }

            public User GetById(int id)
            {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("user/" + id).Result;
                return Response.Content.ReadAsAsync<User>().Result;
            }

            public IEnumerable<User> getMany()
            {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("user").Result;
                return Response.Content.ReadAsAsync<IEnumerable<User>>().Result;
            }

            public User Update(int id, User arg)
            {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PutAsJsonAsync<User>("user/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
                return Response.Content.ReadAsAsync<User>().Result;
            }
        }
    }