﻿using Domain.Entities;
using Service.Interface;
using Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class TopicService : ITopicService<Topic>
    {
        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }
        public TopicService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }
        public Topic Add(Topic arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<Topic>("topic", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Topic>().Result;
        }

        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("topic/" + arg);
        }

        public Topic GetById(int id)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("topic/" + id).Result;
            Topic t= Response.Content.ReadAsAsync<Topic>().Result;
            if (t == null)
                return null;
            t.Comments = client.GetAsync("comment").Result.Content.ReadAsAsync<ICollection<Comment>>().Result.Where(x=>x.TopicId==id).ToList();
            foreach(Comment c in t.Comments)
            {
                
                Response = client.GetAsync("user/" + c.UserId).Result;
                c.User = Response.Content.ReadAsAsync<User>().Result;
            }
            return t;
        }

        public IEnumerable<Topic> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("topic").Result;
            IEnumerable<Topic> to= Response.Content.ReadAsAsync<IEnumerable<Topic>>().Result;
            foreach(Topic item in to)
            {
                
                item.Publisher= client.GetAsync("user/"+item.UserId).Result.Content.ReadAsAsync<User>().Result;
            }
            return to;
        }

        public IEnumerable<Topic> Search(string topic)
        {
            return getMany().Where(t => t.Title.Contains(topic)).ToList();
        }

        public Topic Update(int id, Topic arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PutAsJsonAsync<Topic>("topic/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Topic>().Result;
        }



       


    }
}