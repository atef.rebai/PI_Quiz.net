﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class CertificateService : ICertificateService<Certificate>
    {
        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }


        public CertificateService()
        {

            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public IEnumerable<Certificate> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("lesson").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Certificate>>().Result;
        }

        public IEnumerable<Certificate> getCertificationByLesson(string titre)
        {

            return getMany().Where<Certificate>(c => c.Lesson.Titre == titre).ToList<Certificate>();
        }

        public IEnumerable<Certificate> getCertificationByUser(string name)
        {
            return getMany().Where<Certificate>(c => c.User.Name == name).ToList<Certificate>();
        }


        IEnumerable<Certificate> ICertificateService<Certificate>.getCertificationByTitle(string title)
        {
            return getMany().Where(c => c.Titre.Contains(title)).ToList();
        }
    }
}