﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Domain.Entities;

namespace Service.Service
{
    public class PollService : IPoll<Poll>
    {
        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }
        public PollService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        public Poll Add(Poll arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);

            Response = client.PostAsJsonAsync<Poll>("poll", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Poll>().Result;
        }

        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("poll/" + arg);
        }
        public Poll GetById(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("poll/" + arg).Result;
            Poll p = Response.Content.ReadAsAsync<Poll>().Result;
            p.Questions = client.GetAsync("question").Result.Content.ReadAsAsync<ICollection<Question>>().Result.Where(x=>x.PostId==p.Id).ToList();
            return p;
        }
        public IEnumerable<Poll> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("poll").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Poll>>().Result;
        }



        public Poll Update(int id, Poll arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PutAsJsonAsync<Poll>("poll/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Poll>().Result;
        }

        public IEnumerable<Poll> getPollByTitle(string title)
        {
            //Response = client.GetAsync("question").Result;
            return getMany().Where(poll => poll.Title == title).ToList();

        }

    }
}