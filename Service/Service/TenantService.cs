﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class TenantService : ITenantService
    {
        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }


        public TenantService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }
        public Tenant Add(Tenant arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<Tenant>("tenant", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Tenant>().Result;
        }

        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("tenant/" + arg);
        }

        public Tenant Get()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("tenant/").Result;
            return Response.Content.ReadAsAsync<Tenant>().Result;
        }

        public IEnumerable<Tenant> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("tenant").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Tenant>>().Result;
        }

        public Tenant Update(Tenant arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PutAsJsonAsync<Tenant>("tenant/", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Tenant>().Result;
        }
    }
}