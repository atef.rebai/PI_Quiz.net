﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;
using System.Net.Http;
using System.Net.Http.Headers;
using Domain.token;

namespace Service.Service
{
    public class AuthService : IAuthService
    {
        HttpClient client;
        HttpResponseMessage Response;
        public AuthService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        JWToken IAuthService.Auth(User arg)
        {
            Response = client.PostAsJsonAsync<User>("auth/auth", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<JWToken>().Result;
        }

       
    }
}