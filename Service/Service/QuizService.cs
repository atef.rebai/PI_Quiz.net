﻿
using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class QuizService : IQuizService<Quiz>
    {



        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }
        public QuizService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public Quiz Add(Quiz arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<Quiz>("quiz", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Quiz>().Result;
        }

        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("quiz/" + arg);
        }

        public Quiz GetById(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("quiz/" + arg).Result;
            return Response.Content.ReadAsAsync<Quiz>().Result;

        }

        public Quiz Update(int id, Quiz arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PutAsJsonAsync<Quiz>("quiz/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Quiz>().Result;
        }




        IEnumerable<Quiz> getQuizByLesson(string titre)
        {
            
            return getMany().Where<Quiz>(q => q.Lesson.Titre == titre).ToList<Quiz>();

        }
        public int score(int id)

        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            return GetById(id).Score;

        }


        public int calculScore()
        {
          //  client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            int sum = 0;


            return sum;
        }



        public IEnumerable<Quiz> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("quiz").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Quiz>>().Result;
        }

        IEnumerable<Quiz> IQuizService<Quiz>.getQuizByDescription(string desc)
        {
            
            if (desc == null) throw new ArgumentNullException("pas d'argume ");
            else
            {
                return getMany().Where(q => q.Description.Contains(desc)).ToList();
            }
        }




    }


}