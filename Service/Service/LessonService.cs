﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class LessonService : ILessonService<Lesson>
    {
        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }

        public LessonService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public Lesson Add(Lesson arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<Lesson>("lesson", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Lesson>().Result;
        }
        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("lesson/" + arg);
        }

        public Lesson Update(int id, Lesson arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PutAsJsonAsync<Lesson>("lesson/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Lesson>().Result;
        }

        public Lesson GetByIdWithQuizs(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("lesson/" + arg).Result;
            Lesson l = Response.Content.ReadAsAsync<Lesson>().Result;
            l.Quizs = client.GetAsync("Quiz/" + arg).Result.Content.ReadAsAsync<ICollection<Quiz>>().Result;
            return l;
        }

        public Lesson GetByIdWithCertificates(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("lesson/" + arg).Result;
            Lesson l = Response.Content.ReadAsAsync<Lesson>().Result;
            l.Certificates = client.GetAsync("Certificate/" + arg).Result.Content.ReadAsAsync<ICollection<Certificate>>().Result;
            return l;
        }
      

         IEnumerable<Lesson> ILessonService<Lesson>.getLessonByTitre(string titre)
        {
            
            if (titre == null) throw new ArgumentNullException("Pas d'arguments");
            else
            {
                return getMany().Where(l => l.Titre.Contains(titre)).ToList();
            }
            
        }
        

        public IEnumerable<Lesson> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);

            Response = client.GetAsync("lesson").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Lesson>>().Result;
        }



    }
}