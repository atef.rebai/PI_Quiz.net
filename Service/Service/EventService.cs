﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class EventService : IEventService
    {
        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }
        public EventService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public Event Add(Event arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response =client.PostAsJsonAsync<Event>("event", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Event>().Result;
        }

        public EventComment AddCommentToEvent(EventComment arg,int id)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            arg.Id = 0;
            arg.EventId = id;
            Response =client.PostAsJsonAsync<EventComment>("eventcomment", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result; 
            return Response.Content.ReadAsAsync<EventComment>().Result;

        }

        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("event/" + arg);
        }

        public Event GetById(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("event/"+arg).Result;
            Event e =Response.Content.ReadAsAsync<Event>().Result;
            if (e == null)
                return null;
            e.User= client.GetAsync("user/" + e.UserId).Result.Content.ReadAsAsync<User>().Result;
            e.EventComments = client.GetAsync("eventComment/").Result.Content.ReadAsAsync<ICollection<EventComment>>().Result.Where(ev=>ev.EventId==arg).ToList();
            foreach(EventComment item in e.EventComments)
            {
                item.User= client.GetAsync("user/" + e.UserId).Result.Content.ReadAsAsync<User>().Result;
            }
            return e;
        }

        public IEnumerable<Event> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization",this.TokenHeader);
            Response = client.GetAsync("event").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Event>>().Result;
        }

        public Event Update(int id,Event arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response =client.PutAsJsonAsync<Event>("event/"+id,arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
           return Response.Content.ReadAsAsync<Event>().Result;
        }



        public IEnumerable<Event> getByLocationOrName(string search)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("event").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Event>>().Result.Where(x=>x.Location.Contains(search) || x.Title.Contains(search));

        }

    }
}