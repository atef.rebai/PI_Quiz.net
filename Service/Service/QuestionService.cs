﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class QuestionService : IQuestion<Question>
    {

        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }
        public QuestionService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public Question Add(Question arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<Question>("Question", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Question>().Result;
        }

        public void Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("question/" + arg);
        }

        public Question GetById(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("question/" + arg).Result;
            Question p = Response.Content.ReadAsAsync<Question>().Result;
            return p;
        }

        public IEnumerable<Question> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("question").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Question>>().Result;
        }

        public IEnumerable<Question> getQuestionByPoll(int idpost)
        {
            //Response = client.GetAsync("question").Result;
            return getMany().Where(ques => ques.PostId == idpost).ToList();
            
        }

        public Question Update(int id, Question arg)
        {
            Response = client.PutAsJsonAsync<Question>("question/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Question>().Result;
        }


        public Question AddQuestionToPoll(Question arg, int id)
        {

            arg.Id = 0;
            arg.PostId = id;
            Response = client.PostAsJsonAsync<Question>("question", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Question>().Result;

        }

    }
}