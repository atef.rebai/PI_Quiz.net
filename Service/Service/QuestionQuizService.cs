﻿using Domain.Entities;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Service.Service
{
    public class QuestionQuizService : IQuestionQuizService<Question>
    {

        HttpClient client;
        HttpResponseMessage Response;
        public string TokenHeader { get; set; }

        public QuestionQuizService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        Question IQuestionQuizService<Question>.Add(Question arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.PostAsJsonAsync<Question>("question", arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Question>().Result;
        }

        void IQuestionQuizService<Question>.Delete(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            client.DeleteAsync("question/" + arg);
        }

        public Question GetById(int arg)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("question/" + arg).Result;
            return Response.Content.ReadAsAsync<Question>().Result;
        }

        public IEnumerable<Question> getMany()
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            Response = client.GetAsync("question").Result;
            return Response.Content.ReadAsAsync<IEnumerable<Question>>().Result;
        }

        Question IQuestionQuizService<Question>.Update(int id, Question arg)
        {
            Response = client.PutAsJsonAsync<Question>("question/" + id, arg).ContinueWith(posttask => posttask.Result.EnsureSuccessStatusCode()).Result;
            return Response.Content.ReadAsAsync<Question>().Result;
        }


        IEnumerable<Question> IQuestionQuizService<Question>.getQuisByQuiz(int idQuiz)
        {
            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            return getMany().Where(ques => ques.PostId == idQuiz).ToList();
        }

        public int nbrQuestionByQuiz(int idqz)
        {

            client.DefaultRequestHeaders.Add("Authorization", this.TokenHeader);
            return getMany().Where(x => x.PostId.Equals(idqz)).Count();


        }
    }

}