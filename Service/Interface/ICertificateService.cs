﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    interface ICertificateService<T> where T : Certificate
    {
        string TokenHeader { get; set; }

        IEnumerable<Certificate> getCertificationByTitle(string title);
        IEnumerable<Certificate> getCertificationByUser(string name);
        IEnumerable<Certificate> getCertificationByLesson(string titre);
    }
}
