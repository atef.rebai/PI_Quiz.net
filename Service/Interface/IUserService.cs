﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    interface IUserService <User>
    {
        string TokenHeader { get; set; }
        User Add(User arg);
        void Delete(int arg);
        User Update(int id, User arg);
        IEnumerable<User> getMany();
        User GetById(int id);
    }
}
