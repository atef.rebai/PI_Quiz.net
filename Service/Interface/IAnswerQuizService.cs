﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IAnswerQuizService<T> where T : Answer
    {
        string TokenHeader { get; set; }
        Answer Add(Answer arg);
        void Delete(int arg);
        Answer Update(int id, Answer arg);
        IEnumerable<Answer> getMany();
        Answer GetById(int id);
        IEnumerable<Answer> getAnsByQuestion(int idQuestion);
        //      string GetQuestionCententForAns(int idSouel);

        Answer CreateAByQues(Answer an, int idQuest);

    }
}
