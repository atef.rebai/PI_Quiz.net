﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;


namespace Service.Interface
{
  public  interface IPoll<T> where T : Poll
    {
        string TokenHeader { get; set; }
        Poll Add(Poll arg);
        void Delete(int arg);
        Poll Update(int id, Poll arg);
        IEnumerable<Poll> getMany();
        Poll GetById(int id);
        IEnumerable<Poll> getPollByTitle(string title);


    }
}
