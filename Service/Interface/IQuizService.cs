﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IQuizService<T> where T : Quiz
    {
        string TokenHeader { get; set; }
        IEnumerable<Quiz> getQuizByDescription(string desc);
        //    IEnumerable<Quiz> getMany();

        //System.Web.Helpers.Chart Chart();



    }
}
