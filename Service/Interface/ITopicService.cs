﻿
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    interface ITopicService <T> where T:Topic
    {
        string TokenHeader { get; set; }

        Topic Add(Topic arg);
        void Delete(int arg);
        Topic Update(int id, Topic arg);
        IEnumerable<Topic> getMany();
        Topic GetById(int id);

        IEnumerable<Topic> Search(string topic);


    }
}
