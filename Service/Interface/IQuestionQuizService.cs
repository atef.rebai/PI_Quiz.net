﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IQuestionQuizService<T> where T : Question
    {
        string TokenHeader { get; set; }
        Question Add(Question arg);
        void Delete(int arg);
        Question Update(int id, Question arg);
        IEnumerable<Question> getMany();
        Question GetById(int id);
        IEnumerable<Question> getQuisByQuiz(int idQuiz);
        int nbrQuestionByQuiz(int idqz);
    }
}
