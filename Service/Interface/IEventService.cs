﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Service.Interface
{
    interface IEventService
    {

        string TokenHeader { get; set; }
        Event Add(Event arg);
        void Delete(int arg);
        Event Update(int id,Event arg);
        IEnumerable<Event> getMany();
        Event GetById(int id);
        IEnumerable<Event> getByLocationOrName(string arg);
        EventComment AddCommentToEvent(EventComment arg, int id);
    }
}
