﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service.Interface
{
    public interface ITenantService
    {

        string TokenHeader { get; set; }
        Tenant Add(Tenant arg);
        void Delete(int arg);
        Tenant Update(Tenant arg);
        IEnumerable<Tenant> getMany();
        Tenant Get();
    }
}
