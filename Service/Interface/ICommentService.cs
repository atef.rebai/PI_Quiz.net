﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    interface ICommentService<Comment>
    {
        string TokenHeader { get; set; }
        Comment Add(Comment arg);
        void Delete(int arg);
        Comment Update(int id, Comment arg);
        IEnumerable<Comment> getMany();
        Comment GetById(int id);
    }
}
