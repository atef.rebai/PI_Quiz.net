﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    interface ILessonService<T> where T : Lesson
    {
        string TokenHeader { get; set; }

        IEnumerable<Lesson> getLessonByTitre(string titre);
        


    }
}
