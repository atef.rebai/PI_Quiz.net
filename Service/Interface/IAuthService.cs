﻿using Domain.Entities;
using Domain.token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IAuthService
    {
        JWToken Auth(User arg);
    }
}
