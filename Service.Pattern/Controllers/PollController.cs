﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class PollController : ApiController
    {
        IService<Poll> s = new Service<Poll>(new UnitOfWork(new DataBaseFactory()));
        IService<User> ServiceUser = new Service<User>(new UnitOfWork(new DataBaseFactory()));

        // GET: api/Poll
        [JwtAuthentication]
        public List<Poll> Get()
        {

            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            List<User> listusers = ServiceUser.GetMany().Where(x => x.TenantId == tenantid).ToList();
            return s.GetMany().Where(x => listusers.Any(u => u.Id == x.PublisherId)).ToList<Poll>();
        }

        // GET: api/Poll/5
        [JwtAuthentication]
        public Poll Get(int id)
        {

            int UserId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            Poll ev = s.GetById(id);
            if (ev == null)
                return null;
            User u = ServiceUser.GetById(ev.PublisherId);
            if (u.TenantId == UserId)
                return ev;
            return null;


           
        }

        // POST: api/Poll
        [JwtAuthentication]
        public Poll Post(Poll p)
        {
            p.Id = 0;
            p.PublisherId= Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            p.StartDate = DateTime.Now;
            s.Add(p);
            s.Commit();
            return p;

        }

        // PUT: api/Poll/5
        [JwtAuthentication]
        public Poll Put(int id, Poll p)
        {

            int tokenuserid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);

            Poll instance = s.GetById(id);
            if (instance.PublisherId == tokenuserid || (HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("role").Value == "admin")
            {

            instance.Title = p.Title;
            instance.Description = p.Description;
            instance.StartDate = p.StartDate;
            instance.EndDate = p.EndDate;


                s.Update(instance);
                s.Commit();
                return instance;
            }
            return null;

        }

        // DELETE: api/Poll/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            int Id = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            Poll ev = s.GetById(id);
            if (ev.PublisherId == Id)
            {
                s.Delete(ev);
                s.Commit();
            }
        }
    }
}
