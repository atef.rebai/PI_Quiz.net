﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class EventController : ApiController
    {
        IService<Event> s = new Service<Event>(new UnitOfWork(new DataBaseFactory()));
        IService<User> ServiceUser = new Service<User>(new UnitOfWork(new DataBaseFactory()));


        // GET: api/Event
        [JwtAuthentication]
        public List<Event> Get()
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            List<User> listusers = ServiceUser.GetMany().Where(x => x.TenantId == tenantid).ToList();
            return s.GetMany().Where(x => listusers.Any(u => u.Id == x.UserId)).ToList();

        }

        // GET: api/Event/5
        [JwtAuthentication]
        public Event Get(int id)
        {
            int TenantId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            Event ev = s.GetById(id);
            if (ev == null)
                return null;
            User u = ServiceUser.GetById(ev.UserId);
            if (u.TenantId == TenantId)
                return ev;
            return null;
        }

        // POST: api/Event
        [JwtAuthentication]
        public Event Post(Event ev)
        {
            ev.Id = 0;
            //changed by token
            // ev.UserId = 1;

            ev.UserId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            ev.DateCreation = DateTime.Now;
            s.Add(ev);
            s.Commit();
            return ev;
        }

        // PUT: api/Event/5
        [JwtAuthentication]
        public Event Put(int id, Event ev)
        {
            int tokenuserid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);

            Event instance = s.GetById(id);
            if (instance.UserId == tokenuserid || (HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("role").Value == "admin")
            {
                instance.Title = ev.Title;
               
                instance.Content = ev.Content;
               
                instance.EndDate = ev.EndDate;

                s.Update(instance);
                s.Commit();
                return instance;
            }
            return null;
        }

        // DELETE: api/Event/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            int Id = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            Event ev = s.GetById(id);
            if (ev.UserId == Id)
            {
                s.Delete(ev);
                s.Commit();
            }



        }
    }
}
