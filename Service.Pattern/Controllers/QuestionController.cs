﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class QuestionController : ApiController
    {
        IService<Question> s = new Service<Question>(new UnitOfWork(new DataBaseFactory()));
        IService<User> ServiceUser = new Service<User>(new UnitOfWork(new DataBaseFactory()));
        // GET: api/Question
        [JwtAuthentication]
        public List<Question> Get()
        {
            return s.GetMany().ToList<Question>();
        }

        // GET: api/Question/5
        [JwtAuthentication]
        public Question Get(int id)
        {
            return s.GetById(id);
        }

        // POST: api/Question
        [JwtAuthentication]
        public Question Post(Question q)
        {
            q.Id = 0;
            s.Add(q);
            s.Commit();
            return q;
        }

        // PUT: api/Question/5
        [JwtAuthentication]
        public Question Put(int id, Question q)
        {
            Question instance = s.GetById(id);

            instance.Content = q.Content;
            instance.IsMulti = q.IsMulti;
            instance.Point = q.Point;
            instance.type = q.type;
            instance.ImageURI = q.ImageURI;
            instance.VideoURI = q.VideoURI;

            s.Update(instance);
            s.Commit();
            return instance;
        }

        // DELETE: api/Question/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            s.Delete(s.GetById(id));
            s.Commit();
        }
    }
}
