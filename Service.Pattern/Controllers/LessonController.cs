﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;

using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class LessonController : ApiController
    {
        IService<Lesson> s = new Service<Lesson>(new UnitOfWork(new DataBaseFactory()));
        // GET: api/Lesson
        [JwtAuthentication]
        public List<Lesson> Get()
        {
            return s.GetMany().ToList<Lesson>();
        }
        //public string Get()
        //{
        //    Lesson l = new Lesson
        //    {


        //        Titre = "ccna4",
        //        URIImage = "C://safasafa/jiji",
        //        URIVideo = "C://safasafa/jiji"

        //    };
        //    s.Add(l);
        //    s.Commit();
        //    return "safa";
        //}


        // GET: api/Lesson/5
        [JwtAuthentication]
        public Lesson Get(int id)
        {
            return s.GetById(id);
        }

        // POST: api/Lesson        
        [JwtAuthentication]
        public Lesson Post(Lesson l)
        {

            l.Id = 0;
            s.Add(l);
            s.Commit();
            return l;
        }

        // PUT: api/Lesson/5
        [JwtAuthentication]
        public Lesson Put(int id, Lesson l)
        {
            Lesson instance = s.GetById(id);
            instance.Titre = l.Titre;
            instance.URIImage = l.URIImage;
            instance.URIVideo = l.URIVideo;
            s.Update(instance);
            s.Commit();
            return instance;
        }

        // DELETE: api/Lesson/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            s.Delete(s.GetById(id));
            s.Commit();

        }
    }
}