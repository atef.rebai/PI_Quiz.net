﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class AnswerController : ApiController
    {
        IService<Answer> ans = new Service<Answer>(new UnitOfWork(new DataBaseFactory()));
        // GET: api/Answer
        public string Get()
        {
            Answer an = new Answer
            {

                Content = "ca va ? ",
                IsCorrect = true,
                ImageURI = "http://jiji.com/img/haha3.jpg",
                QuestionId = 8 
             
                
              //  QuestionId = 1,
           //   Question = new List<Question> { }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
       
            };
            ans.Add(an);
            ans.Commit();
            return "jiji";

            //  return ans.GetMany().ToList<Answer>();
        }

        // GET: api/Answer/5
        public Answer Get(int id)
        {
            return ans.GetById(id);
        }

        // POST: api/Answer
        public void Post(Answer an)
        {

          
            ans.Add(an);
            ans.Commit();
           
        }

        // PUT: api/Answer/5
        public void Put(int id, Answer a)
        {
            Answer jaweb = ans.GetById(id);
            jaweb.Content = a.Content;
            jaweb.ImageURI = a.ImageURI;
            jaweb.IsCorrect = a.IsCorrect;
            jaweb.Question = a.Question;
            jaweb.QuestionId = a.QuestionId;
            jaweb.Users = a.Users;
           
            ans.Update(jaweb);
           ans.Commit();
        }

        // DELETE: api/Answer/5
        public void Delete(int id)
        {
            ans.Delete(ans.GetById(id));
            ans.Commit();
        }
    }
}
