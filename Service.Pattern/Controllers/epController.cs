﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
   
    [RoutePrefix("api/ep")]
    public class epController : ApiController
    {
        IService<EventParticipations> s = new Service<EventParticipations>(new UnitOfWork(new DataBaseFactory()));


        // PUT: api/EventParticipation/confirm/201843140406-2
        [HttpGet]
        [Route("confirm/{id}")]
        public EventParticipations confirm(String id)
        {
            EventParticipations ep = s.GetMany(x => x.ActivationKey.Equals(id) && x.State.Equals("pending")).FirstOrDefault();
            if (ep == null)
                return null;

            ep.State = "active";
            s.Update(ep);
            s.Commit();
            return ep;
        }


        // PUT: api/EventParticipation/validate/201843140406-2
        [HttpGet]
        [Route("validate/{id}")]
        public string validate(string id)
        {
            EventParticipations ep = s.GetMany(x => x.ActivationKey.Equals(id)).FirstOrDefault();
            if (ep == null)
                return "no participation was found";
            if (ep.State.Equals("pending"))
                return "you must confirm your participation with mail first";

            return "success accept this one";
        }
    }
}
