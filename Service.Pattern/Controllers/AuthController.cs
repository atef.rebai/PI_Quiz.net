﻿using Domain.Entities;
using Domain.token;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class AuthController : ApiController
    {

        IService<User> s = new Service<User>(new UnitOfWork(new DataBaseFactory()));
        IService<Tenant> stenant = new Service<Tenant>(new UnitOfWork(new DataBaseFactory()));

        // POST: api/Auth
        [HttpPost]
        public JWToken Post(User user)
        {
            User u = s.Get(x => x.username.Equals(user.username) && x.Password.
               Equals(user.Password) && x.Role.Equals("admin"));

            if (u == null)
                return new JWToken() { Token = null };
            else
            {
                u.Tenant = stenant.GetById(u.TenantId);
                return new JWToken() {Token=JwtManager.GenerateToken(u, 43200) } ;


            }
        }

        [HttpPost]
        public JWToken simpleAuth(User user)
        {
            User u = s.Get(x => x.username.Equals(user.username) && x.Password.
               Equals(user.Password) && x.Role.Equals("user") && x.State.Equals("active"));

            if (u == null)
                return new JWToken() { Token = null };
            else
            {
                u.Tenant = stenant.GetById(u.TenantId);
                return new JWToken() { Token = JwtManager.GenerateToken(u, 43200) };


            }
        }

        // get
        [JwtAuthentication]
        public User Get()
        {

            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                return s.GetById(Int32.Parse(identity.FindFirst("Id").Value));
               // return identity.Name + ":" + identity.FindFirst("TenantId").Value;


            }
            return null;
        }
    }
}
