﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    [JwtAuthentication]
    public class CommentController : ApiController
    {
        IService<Comment> s = new Service<Comment>(new UnitOfWork(new DataBaseFactory()));

        // GET: api/Comment
        public List<Comment> Get()
        {
            
            return s.GetMany().ToList<Comment>();
        }

        // GET: api/Comment/5
        public Comment Get(int id)
        {
            return s.GetById(id);
        }

        // POST: api/Comment
        public Comment Post(Comment c)
        {
            c.Id = 0;
            c.UserId=  Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            c.DatePost = DateTime.Now;
            s.Add(c);
            s.Commit();
            return c;
        }

        // PUT: api/Comment/5
        public Comment Put(int id, Comment c)
        {
            Comment instance = s.GetById(id);

            instance.Content = c.Content;
            


            s.Update(instance);
            s.Commit();
            return instance;
        }

        // DELETE: api/Comment/5
        public void Delete(int id)
        {
            s.Delete(s.GetById(id));
            s.Commit();
        }
    }
}
