﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class QuizController : ApiController
    {
        IService<Quiz> qz = new Service<Quiz>(new UnitOfWork(new DataBaseFactory()));
        IService<User> ServiceUser = new Service<User>(new UnitOfWork(new DataBaseFactory()));


        [JwtAuthentication]
        // GET: api/Quiz
        public List<Quiz> Get()
        {
            return qz.GetMany().ToList<Quiz>();
        }
        [JwtAuthentication]
        // GET: api/Quiz/5
        public Quiz Get(int id)
        {
            return qz.GetById(id);
        }
        [JwtAuthentication]
        // POST: api/Quiz
        public void Post(Quiz q )
        {
            //// q.Id = 0;
            //q.Title = "titleupdeted";
            //q.Score = 3;
            q.PublisherId =   Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            //q.StartDate = new DateTime(2017, 02, 02);
            qz.Add(q);
            qz.Commit();

           

        }
        [JwtAuthentication]
        // PUT: api/Quiz/5
        public void Put(int id, Quiz q)
        {
            Quiz quizz = qz.GetById(id);

            Quiz q2 = new Quiz()

            {
                Description = "mon premier quiz"
                
            };

            quizz.Description = q.Description;
            quizz.Duration = q.Duration;
            quizz.Score = q.Score;
            //  quizz.Questions = q.Questions; 

            qz.Update(quizz);
            qz.Commit();
          
        }
        [JwtAuthentication]
        // DELETE: api/Quiz/5
        public void Delete(int id)
        {
            qz.Delete(qz.GetById(id));
            qz.Commit();
        }
    }
}
