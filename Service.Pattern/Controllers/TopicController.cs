﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class TopicController : ApiController
    {
        IService<Topic> s = new Service<Topic>(new UnitOfWork(new DataBaseFactory()));
        IService<User> ServiceUser = new Service<User>(new UnitOfWork(new DataBaseFactory()));
        // GET: api/Topic
        [JwtAuthentication]
        public List<Topic> Get()
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            List<User> listusers = ServiceUser.GetMany().Where(x => x.TenantId == tenantid).ToList();
            return s.GetMany().Where(x => listusers.Any(u=>u.Id==x.UserId)).ToList<Topic>();
        }

        // GET: api/Topic/5
        [JwtAuthentication]
        public Topic Get(int id)
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            Topic topic= s.GetById(id);
            if (topic == null)
                return null;
            User u = ServiceUser.GetById(topic.UserId);
            if (u.TenantId == tenantid)
                return topic;
            return null;
        }

        // POST: api/Topic
        [JwtAuthentication]
        public Topic Post(Topic top)
        {
            
           
            top.Id = 0;
            top.UserId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            top.CreatedOn = DateTime.Now;
            s.Add(top);
            s.Commit();
            return top;
        }

        // PUT: api/Topic/5
        [JwtAuthentication]
        public Topic Put(int id, Topic top)
        {
            int tokenuserid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            Topic instance = s.GetById(id);
            
           
            if(instance.UserId== tokenuserid || (HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("role").Value=="admin") { 

            instance.Title = top.Title;
            instance.Body = top.Body;
            instance.CreatedOn = top.CreatedOn;
            instance.ImageURI = top.ImageURI;


            s.Update(instance);
            s.Commit();
            return instance;
        }
        return null;

    }

        // DELETE: api/Topic/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            int Id = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            Topic t = s.GetById(id);
            if (t.UserId == Id)
            {

                s.Delete(t);
                s.Commit();

            }
            
        }
    }
}
