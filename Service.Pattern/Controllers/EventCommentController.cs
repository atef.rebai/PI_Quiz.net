﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    [JwtAuthentication]
    public class EventCommentController : ApiController
    {
        IService<EventComment> s = new Service<EventComment>(new UnitOfWork(new DataBaseFactory()));

        // GET: api/EventComment
        public List<EventComment> Get()
        {
            return s.GetMany().ToList<EventComment>();
        }

        // GET: api/EventComment/id
        public EventComment Get(int id)
        {
            return s.GetById(id);
        }


        // POST: api/Event
        public EventComment Post(EventComment ev)
        {
            ev.Id = 0;
            ev.UserId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            ev.CreationDate = DateTime.Now;
            s.Add(ev);
            s.Commit();
            return ev;
        }


        // DELETE: api/Event/5
        public void Delete(int id)
        {
            s.Delete(s.GetById(id));
            s.Commit();
        }
    }
}
