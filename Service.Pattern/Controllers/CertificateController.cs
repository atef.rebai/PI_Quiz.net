﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class CertificateController : ApiController
    {
        IService<Certificate> s = new Service<Certificate>(new UnitOfWork(new DataBaseFactory()));
        //GET: api/Certificate
        [JwtAuthentication]
        public List<Certificate> Get()
        {
            return s.GetMany().ToList<Certificate>();
        }
        // public string Get()
        //{

        //    Certificate certif = new Certificate
        //    {
        //        Titre = "haina",
        //        LessonId = 1 ,
        //        UserId = 1 
        //    };
        //     s.Add(certif);
        //    s.Commit();
        //    return "safa";
        //}


        // GET: api/Certificate/5
        [JwtAuthentication]
        public Certificate Get(int id)
        {
            return s.GetById(id);
        }

        // POST: api/Certificate
        [JwtAuthentication]
        public Certificate Post(Certificate c)
        {
           
            c.Id = 0;
            s.Add(c);
            s.Commit();
            return c;
        }

        // PUT: api/Certificate/5
        [JwtAuthentication]
        public Certificate Put(int id, Certificate c)
        {
            Certificate instance = s.GetById(id);
            instance.Titre = c.Titre;
            instance.Date = c.Date;
            s.Update(instance);
            s.Commit();
            return instance;
        }

        // DELETE: api/Certificate/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            s.Delete(s.GetById(id));
            s.Commit();

        }
    }
}