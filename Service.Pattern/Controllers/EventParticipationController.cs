﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    [JwtAuthentication]
    public class EventParticipationController : ApiController
    {
        IService<EventParticipations> s = new Service<EventParticipations>(new UnitOfWork(new DataBaseFactory()));
        IService<Event> event_service = new Service<Event>(new UnitOfWork(new DataBaseFactory()));
        IService<User> User_service = new Service<User>(new UnitOfWork(new DataBaseFactory()));

        // GET: api/EventParticipation
        //returns all my participations
        public List<EventParticipations> Get()
        {
            int UsertId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            return s.GetMany(x=>x.UserId==UsertId).ToList();
        }

        // GET: api/EventParticipation/5
        //get participations for event 5
        public List<EventParticipations> Get(int id)
        {
            int TenantId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            Event ev = event_service.GetById(id);
            if (ev == null)
                return null;
            User u = User_service.GetById(ev.UserId);
            if (u.TenantId != TenantId)
                return null;

            return s.GetMany(x => x.EventId == id).ToList();
        }

        // POST: api/EventParticipation/5
        //create particiation in the event 5
        public EventParticipations Post(int id)
        {
            int UserId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            int TenantId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);

            Event ev = event_service.GetById(id);
            if (ev == null)
                return null;
            User u = User_service.GetById(ev.UserId);
            if (u.TenantId != TenantId)
                return null;

            try { 
            
            EventParticipations p = new EventParticipations()
            {
                EventId = id,
                UserId = UserId,
                CreationDate = DateTime.Now,
                State = "pending",
                ActivationKey = DateTime.Now.ToString("yyyymmddMMss") + "-" + (id + UserId),
            };
            s.Add(p);

            s.Commit();
            return p;
            }catch(Exception e)
            {
                return null;
            }
        }

       



        // DELETE: api/EventParticipation/5
        public void Delete(int id)
        {
            int UserId = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("Id").Value);
            s.Delete(s.GetMany(x=>x.UserId==UserId && x.EventId==id).FirstOrDefault());
            s.Commit();

        }
    }
}
