﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{

    public class UserController : ApiController
    {
        IService<User> s = new Service<User>(new UnitOfWork(new DataBaseFactory()));
        public UserController()
        {

        }
        // GET: api/User
        [JwtAuthentication]
        public List<User> Get()
        {
            int tenantid= Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            return s.GetMany().Where(x=>x.TenantId==tenantid).ToList<User>(); ;
        }

        // GET: api/User/5
        [JwtAuthentication]
        public User Get(int id)
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            User u= s.GetById(id);
            if (u.TenantId == tenantid)
                return u;
            else
                return null;
        }

        // POST: api/User
        [JwtAuthentication]
        public User Post(User u)
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);

            u.Id = 0;
            u.TenantId = tenantid;
            s.Add(u);
            s.Commit();
            return u;
        }

        // PUT: api/User/5
        [JwtAuthentication]
        public User Put(int id, User u)
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            User instance = s.GetById(id);
            if (instance.TenantId == tenantid) { 
            instance.Name = u.Name;
            instance.LastName = u.LastName;
            instance.Password = u.Password;
            instance.ProfileImageURL = u.ProfileImageURL;
            instance.Role = u.Role;
            instance.State = u.State;
            instance.Email = u.Email;
            s.Update(instance);
            s.Commit();
            return instance;
            }
            return null;




        }

        // DELETE: api/User/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            User u = s.GetById(id);
                if (u.TenantId == tenantid)
            {
                s.Delete(u);
                s.Commit();
            }
           
        }
    }
}
