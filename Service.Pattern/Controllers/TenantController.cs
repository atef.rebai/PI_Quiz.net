﻿using Domain.Entities;
using PI_Quiz.Data.Infrastructure;
using Service.Pattern.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Service.Pattern.Controllers
{
    public class TenantController : ApiController
    {
        IService<Tenant> s = new Service<Tenant>(new UnitOfWork(new DataBaseFactory()));
        // GET: api/Tenant
        [JwtAuthentication]
        public Tenant Get()
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            return s.GetById(tenantid);
        }




        public Tenant Post(Tenant t)
        {
            Tenant ten = new Tenant()
            {
                Id = 1,
                Name = "esprit",
                WebSiteURL = "www.esprit.tn",
                CustemColor = "",
                LogoURI = "1.jpg"
            };

            s.Add(ten);
            s.Commit();

            return ten;
        }



        // PUT: api/Tenant/5
        [JwtAuthentication]
        
        public Tenant Put(Tenant t)
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
             ;
            Tenant instance = s.GetById(tenantid);
            if (t.Name != null)
                instance.Name = t.Name;
            if (t.LogoURI != null)
                instance.LogoURI = t.LogoURI;
            if (t.WebSiteURL != null)
                instance.WebSiteURL = t.WebSiteURL;
            if (t.CustemColor != null)
                instance.CustemColor = t.CustemColor;
            s.Update(instance);
            s.Commit();
            return instance;
        }

        // DELETE: api/Tenant/5
        [JwtAuthentication]
        public void Delete(int id)
        {
            int tenantid = Convert.ToInt32((HttpContext.Current.User.Identity as ClaimsIdentity).FindFirst("TenantId").Value);
            id = tenantid;
            s.Delete(s.GetById(id));
            s.Commit();

        }
    }
}
