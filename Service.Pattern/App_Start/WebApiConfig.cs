﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Service.Pattern
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            //config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling= Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            /*
            config.Routes.MapHttpRoute(
               "confirmParticipation",
               "api/ep/confirm/{id}",
               new { controller = "EventParticipation", action = "confirm" });

            config.Routes.MapHttpRoute(
               "validateParticipation",
               "api/ep/validate/{id}",
               new { controller = "EventParticipation", action = "validate" });

            config.Routes.MapHttpRoute(
              "allParticipation",
              "api/EventParticipation",
              new { controller = "EventParticipation", action = "Get" });
              */

            config.Routes.MapHttpRoute(
              "auth_simpleuser",
              "api/auth/simpleauth/",
              new { controller = "Auth", action = "simpleAuth" });

            config.Routes.MapHttpRoute(
              "auth_admin",
              "api/auth/auth/",
              new { controller = "Auth", action = "Post" });



            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
