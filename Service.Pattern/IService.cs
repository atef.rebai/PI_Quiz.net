﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Pattern
{
    public interface IService<T> where T : class
    {
        void Add(T entity);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> condition);
        void Update(T entity);

        T GetById(long id);
        T GetById(string id);
        T Get(Expression<Func<T, bool>> condition);

        IEnumerable<T> GetMany(Expression<Func<T, bool>> condition = null, Expression<Func<T, bool>> orderby = null);

        void Commit();
        void Dispose();

    }
}
