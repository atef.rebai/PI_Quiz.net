﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Web.UI.WebControls;

namespace Service.Pattern.Filters
{
    public class JwtAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        public string Realm { get; set; }
        public bool AllowMultiple => false;

        public async  Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var request = context.Request;
            var authorization = request.Headers.Authorization;

            if (authorization == null || authorization.Scheme != "Bearer")
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing Jwt Token", request);
                return;
            }
                //return;

            if (string.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing Jwt Token", request);
                return;
            }

            var token = authorization.Parameter;
            var principal = await AuthenticateJwtToken(token);

            if (principal == null) {                 context.ErrorResult = new AuthenticationFailureResult("Invalid token", request);
                return;
            }


            else
                context.Principal = principal;
        }
    



    private static bool ValidateToken(string token, out User u)
    {
            u = new User();
        

        var simplePrinciple = JwtManager.GetPrincipal(token);
        var identity = simplePrinciple?.Identity as ClaimsIdentity;

        if (identity == null)
            return false;

        if (!identity.IsAuthenticated)
            return false;

        var usernameClaim = identity.FindFirst(ClaimTypes.Name);
        u.username = usernameClaim?.Value;

            var RoleClaim = identity.FindFirst(ClaimTypes.Role);
            u.Role = RoleClaim?.Value;


            var TenantIdClaim = identity.FindFirst("TenantId");
            u.TenantId = Convert.ToInt32(TenantIdClaim?.Value);

            var IdClaim = identity.FindFirst("Id");
            u.Id = Convert.ToInt32(IdClaim?.Value);

           




            if (string.IsNullOrEmpty(u.username))
            return false;

        // More validate to check whether username exists in system

        return true;
    }

    protected Task<IPrincipal> AuthenticateJwtToken(string token)
    {
            User u;

        if (ValidateToken(token, out u ))
        {
                // based on username to get more information from database in order to build local identity
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, u.username) ,
                    new Claim(ClaimTypes.Role,u.Role) ,
                    new Claim("TenantId", u.TenantId.ToString()),
                    new Claim("Id", u.Id.ToString())
                    // Add more claims if needed: Roles, ...
                };

            var identity = new ClaimsIdentity(claims, "Jwt");
            IPrincipal user = new ClaimsPrincipal(identity);

            return Task.FromResult(user);
        }

        return Task.FromResult<IPrincipal>(null);
    }
    public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Challenge(context);
            return Task.FromResult(0);
        }
        private void Challenge(HttpAuthenticationChallengeContext context)
        {
            string parameter = null;

            if (!string.IsNullOrEmpty(Realm))
                parameter = "realm=\"" + Realm + "\"";

            context.ChallengeWith("Bearer", parameter);
        }
    }
}
