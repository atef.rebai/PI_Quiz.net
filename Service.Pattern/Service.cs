﻿using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using PI_Quiz.Data.Infrastructure;

namespace Service.Pattern
{
    public class Service<T> : IService<T> where T : class
    {
        IUnitOfWork unit;

        public Service(IUnitOfWork unit)
        {
            this.unit = unit;
        }

        public void Add(T entity)
        {
            unit.GetRepository<T>().Add(entity);
        }

        public void Delete(Expression<Func<T, bool>> condition)
        {
            unit.GetRepository<T>().Delete(condition);
        }

        public void Delete(T entity)
        {
            unit.GetRepository<T>().Delete(entity);
        }

        public T Get(Expression<Func<T, bool>> condition)
        {
            return unit.GetRepository<T>().Get(condition);
        }

        public T GetById(string id)
        {
            return unit.GetRepository<T>().GetById(id);
        }

        public T GetById(long id)
        {
            return unit.GetRepository<T>().GetById(id);
        }

        public IEnumerable<T> GetMany(Expression<Func<T, bool>> condition = null, Expression<Func<T, bool>> orderby = null)
        {
            return unit.GetRepository<T>().GetMany(condition,orderby);
        }

        public void Update(T entity)
        {
            unit.GetRepository<T>().Update(entity);
        }

        public void Commit()
        {
            unit.Commit();
        }

        public void Dispose()
        {
            unit.Dispose();
        }
    }
}
